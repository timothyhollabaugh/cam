## Generate G-Code for PCBs

This will generate the g-code required for milling out PCBs.

![screenshot](screenshot.png)

## Features

 - Clear all unused copper
 - Save and re-use tool settings
 - All settings saved to disk
 - Choose which tools to use for each operation

## Running

You will need the following:

 - Rust nightly
 - Python
 - Shapely
 - Numpy

Once these are installed, running should be a matter of
```shell script
cargo run --release
```
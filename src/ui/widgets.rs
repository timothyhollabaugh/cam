use crate::cam::geometry::Point;
use crate::cam::job::OpKey;
use crate::settings::{ToolKey, ToolSettings, ToolTable};
use crate::ui::lenses::KeyedLens;
use crate::ui::shared_list::SharedListItem;
use druid::kurbo::PathEl;
use druid::widget::{Button, Flex, List, Scroll};
use druid::{
    lens, theme, BoxConstraints, Data, Env, Event, EventCtx, LayoutCtx, LensExt, LifeCycle,
    LifeCycleCtx, PaintCtx, Rect, RenderContext, Selector, Size, UpdateCtx, Widget, WidgetExt,
    WidgetPod,
};

pub struct Select<T> {
    selected: bool,
    f: Box<dyn Fn(&T, &Env) -> bool>,
    inner: WidgetPod<T, Box<dyn Widget<T>>>,
}

impl<T: Data> Select<T> {
    pub fn new(f: impl Fn(&T, &Env) -> bool + 'static, inner: impl Widget<T> + 'static) -> Self {
        Select {
            selected: false,
            f: Box::new(f),
            inner: WidgetPod::new(inner).boxed(),
        }
    }
}

impl<T: Data> Widget<T> for Select<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.inner.event(ctx, event, data, env)
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        self.inner.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        let selected = (self.f)(data, env);

        if selected != self.selected {
            ctx.request_paint();
            ctx.request_layout();
            self.selected = selected;
        }

        self.inner.update(ctx, data, env)
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        let size = self.inner.layout(ctx, bc, data, env);
        self.inner
            .set_layout_rect(ctx, data, env, Rect::from_origin_size((0.0, 0.0), size));

        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        if self.selected {
            let size = ctx.size();
            ctx.fill(
                Rect::from((Point { x: 0.0, y: 0.0 }.into(), size)),
                &env.get(theme::SELECTION_COLOR),
            );
        }
        self.inner.paint(ctx, data, env);
    }
}

pub struct Collapse<T> {
    collapsed: bool,
    title: WidgetPod<T, Box<dyn Widget<T>>>,
    inner: WidgetPod<T, Box<dyn Widget<T>>>,
}

impl<T: Data> Collapse<T> {
    pub fn new(
        title: impl Widget<T> + 'static,
        inner: impl Widget<T> + 'static,
        initial_collapsed: bool,
    ) -> Self {
        Collapse {
            collapsed: initial_collapsed,
            title: WidgetPod::new(title).boxed(),
            inner: WidgetPod::new(inner).boxed(),
        }
    }
}

impl<T: Data> Widget<T> for Collapse<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        let edge_width = env.get(theme::BASIC_WIDGET_HEIGHT);

        match event {
            Event::MouseDown(mouse) => {
                ctx.set_active(true);
                if mouse.pos.x > 0.0
                    && mouse.pos.x < edge_width
                    && mouse.pos.y > 0.0
                    && mouse.pos.y < edge_width
                {
                    self.collapsed = !self.collapsed;
                    ctx.request_layout();
                    ctx.request_paint();
                }
            }
            _ => {}
        }

        self.title.event(ctx, event, data, env);

        if !self.collapsed {
            self.inner.event(ctx, event, data, env);
        }
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        self.title.lifecycle(ctx, event, data, env);
        self.inner.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        self.title.update(ctx, data, env);
        self.inner.update(ctx, data, env)
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        let edge_width = env.get(theme::BASIC_WIDGET_HEIGHT);
        let title_size = self
            .title
            .layout(ctx, &bc.shrink((edge_width, 0.0)), data, env);
        let requested_inner_size =
            self.inner
                .layout(ctx, &bc.shrink((edge_width, title_size.height)), data, env);

        self.title.set_layout_rect(
            ctx,
            data,
            env,
            Rect::from_origin_size((edge_width, 0.0), title_size),
        );
        self.inner.set_layout_rect(
            ctx,
            data,
            env,
            Rect::from_origin_size((edge_width, title_size.height), requested_inner_size),
        );

        let inner_size = if self.collapsed {
            (0.0, 0.0).into()
        } else {
            requested_inner_size
        };

        (
            edge_width + title_size.width.max(inner_size.width),
            title_size.height + inner_size.height,
        )
            .into()
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.title.paint(ctx, data, env);

        if !self.collapsed {
            self.inner.paint(ctx, data, env);
        }

        let edge_width = env.get(theme::BASIC_WIDGET_HEIGHT);
        let border_width = env.get(theme::BUTTON_BORDER_WIDTH);
        let edge_color = env.get(theme::LABEL_COLOR);

        let edge_path = if self.collapsed {
            [
                PathEl::MoveTo(
                    Point {
                        x: border_width + (edge_width - border_width) / 4.0,
                        y: border_width,
                    }
                    .into(),
                ),
                PathEl::LineTo(
                    Point {
                        x: edge_width - border_width - (edge_width - border_width) / 4.0,
                        y: edge_width / 2.0,
                    }
                    .into(),
                ),
                PathEl::LineTo(
                    Point {
                        x: border_width + (edge_width - border_width) / 4.0,
                        y: edge_width - border_width,
                    }
                    .into(),
                ),
            ]
        } else {
            [
                PathEl::MoveTo(
                    Point {
                        x: border_width,
                        y: border_width + (edge_width - border_width) / 4.0,
                    }
                    .into(),
                ),
                PathEl::LineTo(
                    Point {
                        x: edge_width / 2.0,
                        y: edge_width - border_width - (edge_width - border_width) / 4.0,
                    }
                    .into(),
                ),
                PathEl::LineTo(
                    Point {
                        x: edge_width - border_width,
                        y: border_width + (edge_width - border_width) / 4.0,
                    }
                    .into(),
                ),
            ]
        };

        ctx.stroke(&edge_path[..], &edge_color, border_width);
    }
}

// pub const LOAD_GEOMETRY: Selector<OpGeometryKey> = Selector::new("tim.pycut.load_geometry");
//
// pub struct LoadGeometry {
//     op_geometry_key: Option<OpGeometryKey>,
//     widgets: WidgetPod<Option<OpGeometryKey>, Flex<Option<OpGeometryKey>>>,
// }
//
// impl LoadGeometry {
//     pub fn new() -> LoadGeometry {
//         let widgets = Flex::row()
//             .with_flex_child(
//                 Button::new("Outlined")
//                     .on_click(
//                         |ctx: &mut EventCtx,
//                          op_geometry_key: &mut Option<OpGeometryKey>,
//                          _env: &Env| {
//                             if let Some(key) = op_geometry_key {
//                                 ctx.submit_command(Command::new(
//                                     TO_CAM,
//                                     RequestType::Load(key.clone(), true),
//                                     ctx.window_id(),
//                                 ));
//                                 *op_geometry_key = None;
//                             }
//                         },
//                     )
//                     .expand_width(),
//                 1.0,
//             )
//             .with_flex_child(
//                 Button::new("Filled")
//                     .on_click(
//                         |ctx: &mut EventCtx,
//                          op_geometry_key: &mut Option<OpGeometryKey>,
//                          _env: &Env| {
//                             if let Some(key) = op_geometry_key {
//                                 ctx.submit_command(Command::new(
//                                     TO_CAM,
//                                     RequestType::Load(key.clone(), false),
//                                     ctx.window_id(),
//                                 ));
//                                 *op_geometry_key = None;
//                             }
//                         },
//                     )
//                     .expand_width(),
//                 1.0,
//             );
//
//         LoadGeometry {
//             op_geometry_key: None,
//             widgets: WidgetPod::new(widgets),
//         }
//     }
// }
//
// impl<T: Data> Widget<T> for LoadGeometry {
//     fn event(&mut self, ctx: &mut EventCtx, event: &Event, _data: &mut T, env: &Env) {
//         if self.op_geometry_key.is_some() {
//             self.widgets
//                 .event(ctx, event, &mut self.op_geometry_key, env)
//         }
//         if let Event::Command(cmd) = event {
//             if let Some(key) = cmd.get(LOAD_GEOMETRY) {
//                 self.op_geometry_key = Some(key.clone());
//                 ctx.request_layout();
//                 ctx.request_paint();
//             }
//         }
//     }
//
//     fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, _data: &T, env: &Env) {
//         if self.op_geometry_key.is_some() {
//             self.widgets
//                 .lifecycle(ctx, event, &mut self.op_geometry_key, env)
//         }
//     }
//
//     fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, _data: &T, env: &Env) {
//         if self.op_geometry_key.is_some() {
//             self.widgets.update(ctx, &mut self.op_geometry_key, env)
//         }
//     }
//
//     fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, _data: &T, env: &Env) -> Size {
//         if self.op_geometry_key.is_some() {
//             let size = self.widgets.layout(ctx, bc, &mut self.op_geometry_key, env);
//             self.widgets.set_layout_rect(
//                 ctx,
//                 &mut self.op_geometry_key,
//                 env,
//                 Rect::from_origin_size((0.0, 0.0), size),
//             );
//             size
//         } else {
//             bc.min()
//         }
//     }
//
//     fn paint(&mut self, ctx: &mut PaintCtx, _data: &T, env: &Env) {
//         if self.op_geometry_key.is_some() {
//             self.widgets.paint(ctx, &mut self.op_geometry_key, env)
//         }
//     }
// }

pub const SELECT_TOOL: Selector<ToolTarget> = Selector::new("tim.pycut.select_tool");
pub const SELECTED_TOOL: Selector<(ToolTarget, ToolKey)> = Selector::new("tim.pycut.selected_tool");

#[derive(Clone, Data)]
pub enum ToolTarget {
    Op(OpKey),
}

pub struct ToolSelect {
    target: Option<ToolTarget>,
    widgets: WidgetPod<(Option<ToolTarget>, ToolTable), Flex<(Option<ToolTarget>, ToolTable)>>,
}

type ToolSelectItem = SharedListItem<Option<ToolTarget>, ToolKey, ToolSettings>;

impl ToolSelect {
    pub fn new() -> ToolSelect {
        let widgets = Flex::column()
            .with_child(
                Scroll::new(
                    List::new(|| {
                        Button::new(|item: &ToolSelectItem, _env: &Env| item.value.name.clone())
                            .on_click(
                                |ctx: &mut EventCtx, item: &mut ToolSelectItem, _env: &Env| {
                                    if let Some(target) = &item.shared {
                                        ctx.submit_command(
                                            SELECTED_TOOL.with((target.clone(), item.key.clone())),
                                        );
                                    }
                                    item.shared = None;
                                },
                            )
                            .expand_width()
                    })
                    .lens(KeyedLens::shared(
                        lens!((Option<ToolTarget>, ToolTable), 0),
                        lens!((Option<ToolTarget>, ToolTable), 1).then(ToolTable::tools),
                    )),
                )
                .vertical()
                .fix_height(100.0),
            )
            .with_child(
                Button::new("Cancel")
                    .on_click(
                        |_ctx: &mut EventCtx, target: &mut Option<ToolTarget>, _env: &Env| {
                            *target = None;
                        },
                    )
                    .expand_width()
                    .lens(lens!((Option<ToolTarget>, ToolTable), 0)),
            );

        ToolSelect {
            target: None,
            widgets: WidgetPod::new(widgets),
        }
    }
}

impl Widget<ToolTable> for ToolSelect {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, tools: &mut ToolTable, env: &Env) {
        if let Event::Command(cmd) = event {
            if let Some(target) = cmd.get(SELECT_TOOL) {
                self.target = Some(target.clone());
                ctx.request_layout();
                ctx.request_paint();
            }
        }

        if !self.target.is_none() {
            let mut data = (self.target.to_owned(), tools.to_owned());

            self.widgets.event(ctx, event, &mut data, env);

            let (new_key, new_tools) = data;

            if !new_key.same(&self.target) {
                self.target = new_key;
                ctx.request_layout();
                ctx.request_paint();
            }

            if !new_tools.same(&new_tools) {
                *tools = new_tools;
            }
        }
    }

    fn lifecycle(
        &mut self,
        ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        tools: &ToolTable,
        env: &Env,
    ) {
        self.widgets
            .lifecycle(ctx, event, &(self.target.to_owned(), tools.to_owned()), env);
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        _old_tools: &ToolTable,
        tools: &ToolTable,
        env: &Env,
    ) {
        self.widgets
            .update(ctx, &mut (self.target.to_owned(), tools.to_owned()), env);
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        tools: &ToolTable,
        env: &Env,
    ) -> Size {
        if self.target.is_some() {
            let data = (self.target.to_owned(), tools.to_owned());
            let size = self.widgets.layout(ctx, bc, &data, env);

            self.widgets
                .set_layout_rect(ctx, &data, env, Rect::from_origin_size((0.0, 0.0), size));
            size
        } else {
            bc.min()
        }
    }

    fn paint(&mut self, ctx: &mut PaintCtx, tools: &ToolTable, env: &Env) {
        if self.target.is_some() {
            self.widgets
                .paint(ctx, &(self.target.clone(), tools.clone()), env)
        }
    }
}

use druid::widget::ListIter;
use druid::Data;

use slotmap::{DenseSlotMap, Key, SecondaryMap};

use im::Vector;
use std::fmt::Debug;

pub trait Keyed<K, V: 'static> {
    // This is a GAT, generic associated type, btw
    type Iter<'a>: Iterator<Item = (K, &'a V)>
    where
        Self: 'a,
        V: 'a;

    fn iter(&self) -> Self::Iter<'_>;
    fn get_mut(&mut self, key: K) -> Option<&mut V>;
    fn get(&self, key: K) -> Option<&V>;
    fn remove(&mut self, key: K);
}

impl<K: 'static + Key, V: 'static> Keyed<K, V> for DenseSlotMap<K, V> {
    type Iter<'a> = slotmap::dense::Iter<'a, K, V>;

    fn iter(&self) -> Self::Iter<'_> {
        DenseSlotMap::iter(self)
    }
    fn get_mut(&mut self, key: K) -> Option<&mut V> {
        DenseSlotMap::get_mut(self, key)
    }
    fn get(&self, key: K) -> Option<&V> {
        DenseSlotMap::get(self, key)
    }
    fn remove(&mut self, key: K) {
        DenseSlotMap::remove(self, key);
    }
}

impl<K: 'static + Key, V: 'static> Keyed<K, V> for SecondaryMap<K, V> {
    type Iter<'a> = slotmap::secondary::Iter<'a, K, V>;

    fn iter(&self) -> Self::Iter<'_> {
        SecondaryMap::iter(self)
    }
    fn get_mut(&mut self, key: K) -> Option<&mut V> {
        SecondaryMap::get_mut(self, key)
    }
    fn get(&self, key: K) -> Option<&V> {
        SecondaryMap::get(self, key)
    }
    fn remove(&mut self, key: K) {
        SecondaryMap::remove(self, key);
    }
}

impl<V: 'static> Keyed<usize, V> for Vec<V> {
    type Iter<'v> = std::iter::Enumerate<std::slice::Iter<'v, V>>;

    fn iter(&self) -> Self::Iter<'_> {
        self.as_slice().iter().enumerate()
    }
    fn get_mut(&mut self, key: usize) -> Option<&mut V> {
        self.as_mut_slice().get_mut(key)
    }
    fn get(&self, key: usize) -> Option<&V> {
        self.as_slice().get(key)
    }
    fn remove(&mut self, key: usize) {
        Vec::remove(self, key);
    }
}

fn option_iter_map<K: Clone, V>(i: &(K, V)) -> (K, &V) {
    (i.0.clone(), &i.1)
}

impl<K: 'static + Clone + Debug, V: 'static + Debug> Keyed<K, V> for Option<(K, V)> {
    type Iter<'v> = std::iter::Map<std::option::Iter<'v, (K, V)>, fn(&'v (K, V)) -> (K, &'v V)>;

    fn iter<'v>(&'v self) -> Self::Iter<'v> {
        self.iter().map(option_iter_map)
    }
    fn get_mut(&mut self, _key: K) -> Option<&mut V> {
        self.as_mut().map(|(_, v)| v)
    }
    fn get(&self, _key: K) -> Option<&V> {
        self.as_ref().map(|(_, v)| v)
    }
    fn remove(&mut self, _key: K) {
        *self = None;
    }
}

#[derive(Clone)]
pub struct SharedListItem<S, K, T>
where
    S: Data,
    K: Data,
    T: Data,
{
    pub shared: S,
    pub list_ops: ListOps<K>,
    pub key: K,
    pub value: T,
}

impl<S, K, T> Data for SharedListItem<S, K, T>
where
    S: Data,
    K: Data,
    T: Data,
{
    fn same(&self, other: &Self) -> bool {
        self.shared.same(&other.shared)
            && self.list_ops.same(&other.list_ops)
            && self.key.same(&other.key)
            && self.value.same(&other.value)
    }
}

#[derive(Clone)]
pub struct SharedList<S: Data, K: Data, T: Data> {
    pub shared: S,
    pub list_ops: ListOps<K>,
    pub list: Vector<(K, T)>,
}

impl<S: Data, K: Data, T: Data> Data for SharedList<S, K, T> {
    fn same(&self, other: &Self) -> bool {
        self.shared.same(&other.shared)
            && self.list_ops.same(&other.list_ops)
            && self.list.same(&other.list)
    }
}

impl<S: Data, K: Data, T: Data> ListIter<SharedListItem<S, K, T>> for SharedList<S, K, T> {
    fn for_each(&self, mut cb: impl FnMut(&SharedListItem<S, K, T>, usize)) {
        for (i, (key, value)) in self.list.iter().enumerate() {
            let item = SharedListItem {
                shared: self.shared.to_owned(),
                list_ops: self.list_ops.to_owned(),
                key: key.to_owned(),
                value: value.to_owned(),
            };

            cb(&item, i);
        }
    }

    fn for_each_mut(&mut self, mut cb: impl FnMut(&mut SharedListItem<S, K, T>, usize)) {
        for (i, (key, value)) in self.list.iter_mut().enumerate() {
            let mut item = SharedListItem {
                shared: self.shared.to_owned(),
                list_ops: self.list_ops.to_owned(),
                key: key.to_owned(),
                value: value.to_owned(),
            };

            cb(&mut item, i);

            if !self.shared.same(&item.shared) {
                self.shared = item.shared;
            }

            if !self.list_ops.same(&item.list_ops) {
                self.list_ops = item.list_ops;
            }

            if !key.same(&item.key) {
                *key = item.key;
            }

            if !value.same(&item.value) {
                *value = item.value
            }
        }
    }

    fn data_len(&self) -> usize {
        self.list.len()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ListOp<K: Data> {
    Remove(K),
}

impl<K: Data> Data for ListOp<K> {
    #[allow(irrefutable_let_patterns)]
    fn same(&self, other: &Self) -> bool {
        match self {
            ListOp::Remove(key) => {
                if let ListOp::Remove(other_key) = other {
                    key.same(other_key)
                } else {
                    false
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct ListOps<K: Data> {
    ops: Vector<ListOp<K>>,
}

impl<K: Data> ListOps<K> {
    pub fn new() -> ListOps<K> {
        ListOps { ops: Vector::new() }
    }

    pub fn remove(&mut self, key: K) {
        self.ops.push_back(ListOp::Remove(key));
    }

    pub fn apply<T: 'static, C: Keyed<K, T>>(self, to_update: &mut C) -> bool {
        let len = self.ops.len();
        for op in self.ops.into_iter() {
            match op {
                ListOp::Remove(key) => {
                    to_update.remove(key);
                }
            }
        }

        return len > 0;
    }
}

impl<K: Data> Data for ListOps<K> {
    fn same(&self, other: &Self) -> bool {
        self.ops.same(&other.ops)
    }
}

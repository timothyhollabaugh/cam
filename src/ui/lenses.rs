use std::marker::PhantomData;
use std::sync::Arc;

use druid::{Data, Lens};

use super::shared_list::{Keyed, ListOps, SharedList};

/// A lens that ignores whatever data it has and gives an empty tuple
pub struct EmptyLens;

impl<In> Lens<In, ()> for EmptyLens {
    fn with<R, F: FnOnce(&()) -> R>(&self, _data: &In, f: F) -> R {
        f(&())
    }

    fn with_mut<R, F: FnOnce(&mut ()) -> R>(&self, _data: &mut In, f: F) -> R {
        f(&mut ())
    }
}

/// A lens that applies two lenses to the same data
/// Almost like a parallel version of Then
pub struct ChainLens<In, Lens1, Lens2> {
    lens1: Lens1,
    lens2: Lens2,
    phantom_in: PhantomData<In>,
}

impl<In, Lens1, Lens2> ChainLens<In, Lens1, Lens2> {
    pub fn new(lens1: Lens1, lens2: Lens2) -> ChainLens<In, Lens1, Lens2> {
        ChainLens {
            lens1,
            lens2,
            phantom_in: PhantomData,
        }
    }
}

impl<In, Lens1, Lens2, Out1, Out2> Lens<In, (Out1, Out2)> for ChainLens<In, Lens1, Lens2>
where
    In: Data,
    Lens1: Lens<In, Out1>,
    Lens2: Lens<In, Out2>,
    Out1: Data,
    Out2: Data,
{
    fn with<R, F: FnOnce(&(Out1, Out2)) -> R>(&self, data: &In, f: F) -> R {
        self.lens1.with(data, |out1| {
            self.lens2
                .with(data, |out2| f(&(out1.to_owned(), out2.to_owned())))
        })
    }

    fn with_mut<R, F: FnOnce(&mut (Out1, Out2)) -> R>(&self, data: &mut In, f: F) -> R {
        // Because we cannot apply both lenses mutably to the data twice at the same time,
        // we make two data's, one for each lens
        let mut data1 = data.to_owned();
        let mut data2 = data.to_owned();

        // Now we can apply to two lenses to each of the two data's at the same time
        let result = self.lens1.with_mut(&mut data1, |out1| {
            self.lens2.with_mut(&mut data2, |out2| {
                let mut out = (out1.to_owned(), out2.to_owned());

                let result = f(&mut out);

                if !out.0.same(out1) {
                    *out1 = out.0;
                }

                if !out.1.same(out2) {
                    *out2 = out.1;
                }

                result
            })
        });

        // Now we can go back and take what changed out of the two data's and put them into
        // the original data
        let new_out1 = self.lens1.with(&data1, |out1| out1.to_owned());
        self.lens1.with_mut(data, |out1| {
            if !out1.same(&new_out1) {
                *out1 = new_out1;
            }
        });

        let new_out2 = self.lens2.with(&data2, |out2| out2.to_owned());
        self.lens2.with_mut(data, |out2| {
            if !out2.same(&new_out2) {
                *out2 = new_out2;
            }
        });

        result
    }
}

pub struct IndexKeyedLens<In, Key, KeyLens, MapLens, C> {
    chained_lens: ChainLens<In, KeyLens, MapLens>,
    phantom_key: PhantomData<Key>,
    phantom_in: PhantomData<In>,
    phantom_c: PhantomData<C>,
}

impl<In, Key, KeyLens, MapLens, C> IndexKeyedLens<In, Key, KeyLens, MapLens, C> {
    pub fn new(
        key_lens: KeyLens,
        map_lens: MapLens,
    ) -> IndexKeyedLens<In, Key, KeyLens, MapLens, C> {
        IndexKeyedLens {
            chained_lens: ChainLens::new(key_lens, map_lens),
            phantom_key: PhantomData,
            phantom_in: PhantomData,
            phantom_c: PhantomData,
        }
    }
}

impl<In, Key, Value, KeyLens, MapLens, C> Lens<In, Option<Value>>
    for IndexKeyedLens<In, Key, KeyLens, MapLens, C>
where
    In: Data,
    Key: Data,
    Value: Data,
    KeyLens: Lens<In, Key>,
    MapLens: Lens<In, Arc<C>>,
    C: 'static + Keyed<Key, Value> + Clone,
{
    fn with<R, F: FnOnce(&Option<Value>) -> R>(&self, data: &In, f: F) -> R {
        self.chained_lens.with(data, |(key, map)| {
            let value = map.as_ref().get(key.clone()).cloned();
            f(&value)
        })
    }

    fn with_mut<R, F: FnOnce(&mut Option<Value>) -> R>(&self, data: &mut In, f: F) -> R {
        self.chained_lens.with_mut(data, |(key, map)| {
            let mut new_map = map.as_ref().clone();
            let mut new_value = new_map.get(key.clone()).cloned();
            let result = f(&mut new_value);

            if let (Some(value), Some(new_value)) = (new_map.get_mut(key.clone()), new_value) {
                *value = new_value;
                *map = Arc::new(new_map);
            }

            result
        })
    }
}

pub struct KeyedLens<In, SharedLens, MapLens, C> {
    chained_lens: ChainLens<In, SharedLens, MapLens>,
    phantom_in: PhantomData<In>,
    phantom_c: PhantomData<C>,
}

impl<In, MapLens, C> KeyedLens<In, EmptyLens, MapLens, C> {
    pub fn new(map_lens: MapLens) -> KeyedLens<In, EmptyLens, MapLens, C> {
        KeyedLens {
            chained_lens: ChainLens::new(EmptyLens, map_lens),
            phantom_in: PhantomData,
            phantom_c: PhantomData,
        }
    }
}

impl<In, SharedLens, MapLens, C> KeyedLens<In, SharedLens, MapLens, C> {
    pub fn shared(
        shared_lens: SharedLens,
        map_lens: MapLens,
    ) -> KeyedLens<In, SharedLens, MapLens, C> {
        KeyedLens {
            chained_lens: ChainLens::new(shared_lens, map_lens),
            phantom_in: PhantomData,
            phantom_c: PhantomData,
        }
    }
}

impl<'a, In, Shared, SharedLens, MapLens, K, V, C> Lens<In, SharedList<Shared, K, V>>
    for KeyedLens<In, SharedLens, MapLens, C>
where
    In: Data,
    Shared: Data,
    SharedLens: Lens<In, Shared>,
    MapLens: Lens<In, Arc<C>>,
    K: Data,
    V: Data,
    C: 'static + Keyed<K, V> + Clone,
{
    fn with<R, F: FnOnce(&SharedList<Shared, K, V>) -> R>(&self, data: &In, f: F) -> R {
        self.chained_lens.with(data, |(shared, map)| {
            let new_map = map.as_ref().clone();
            let list = new_map.iter().map(|(k, v)| (k, v.to_owned())).collect();
            let shared_list = SharedList {
                shared: shared.to_owned(),
                list_ops: ListOps::new(),
                list,
            };
            f(&shared_list)
        })
    }

    fn with_mut<R, F: FnOnce(&mut SharedList<Shared, K, V>) -> R>(&self, data: &mut In, f: F) -> R {
        self.chained_lens.with_mut(data, |(shared, map)| {
            let mut new_map = map.as_ref().clone();

            // Get all of the keys and values into a vector list
            let list = new_map.iter().map(|(k, v)| (k, v.to_owned())).collect();

            // Make the shared list with the vector list, the shared data, and a
            // list_ops for changing the list
            let mut shared_list = SharedList {
                shared: shared.to_owned(),
                list_ops: ListOps::new(),
                list,
            };

            // Call the function to see what it does
            let result = f(&mut shared_list);

            // Update the map if anything changed
            let mut map_changed = false;

            // Go through and check if any part of the map changed.
            // If so, update it and mark the map as changed
            for (k, v) in shared_list.list {
                if let Some(d) = new_map.get_mut(k) {
                    if !v.same(d) {
                        *d = v;
                        map_changed = true;
                    }
                }
            }

            // Apply the list operations, and mark the map as changed accordingly
            map_changed |= shared_list.list_ops.apply(&mut new_map);

            // If the map was changed at all, we need to put it in a new arc
            // so druid picks it up as changed and updates widgets
            if map_changed {
                *map = Arc::new(new_map);
            }

            // Update the shared data if it was changed
            if !shared.same(&shared_list.shared) {
                *shared = shared_list.shared
            }

            result
        })
    }
}

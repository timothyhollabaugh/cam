use crate::cam::geometry::{Geometry, MultiLineString, MultiPolygon, Point};
use crate::cam::job::{OpSide, OpType};
use crate::CamState;
use druid::kurbo::{Affine, PathEl, Rect};
use druid::piet::{LineCap, LineJoin, StrokeStyle};
use druid::RenderContext;
use druid::{
    theme, BoxConstraints, Data, Env, Event, EventCtx, LayoutCtx, LifeCycle, LifeCycleCtx,
    PaintCtx, Size, UpdateCtx, Widget,
};
use druid::{Color, MouseButton};
use im::Vector;
use std::f64::consts::FRAC_PI_4;

const LINE_THICKNESS: f64 = 0.010;
const ARROW_LENGTH: f64 = 0.05;
const DASH_LENGTH: f64 = 0.050;

pub struct CamCanvas {
    scale: f64,
    offset: Point,
    mouse_move_start_offset: Option<Point>,
}

impl CamCanvas {
    pub fn new() -> CamCanvas {
        CamCanvas {
            scale: 1.0,
            offset: Point { x: 0.0, y: 0.0 },
            mouse_move_start_offset: None,
        }
    }
}

impl Widget<CamState> for CamCanvas {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, _state: &mut CamState, _env: &Env) {
        match event {
            Event::Wheel(mouse) => {
                let new_scale = if mouse.wheel_delta.y < 0.0 {
                    self.scale * (1.0 + -mouse.wheel_delta.y / 1000.0)
                } else {
                    self.scale / (1.0 + mouse.wheel_delta.y / 1000.0)
                };

                let rel_scale = new_scale / self.scale;

                self.offset *= rel_scale;

                self.scale = new_scale;

                ctx.request_paint();
            }

            Event::MouseDown(mouse) => {
                if mouse.button == MouseButton::Left {
                    self.mouse_move_start_offset = Some(self.offset - mouse.pos.into())
                }
            }

            Event::MouseUp(mouse) => {
                if mouse.button == MouseButton::Left {
                    self.mouse_move_start_offset = None
                }
            }

            Event::MouseMove(mouse) => {
                if let Some(start_offset) = self.mouse_move_start_offset {
                    self.offset = start_offset + Point::from(mouse.pos)
                }
                ctx.request_paint();
            }
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        _state: &CamState,
        _env: &Env,
    ) {
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_state: &CamState, state: &CamState, _env: &Env) {
        if !old_state.same(state) {
            ctx.request_paint();
        }
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _job: &CamState,
        _env: &Env,
    ) -> Size {
        bc.max()
    }

    fn paint(&mut self, ctx: &mut PaintCtx, state: &CamState, env: &Env) {
        let job = &state.job;

        let background_color = env.get(theme::BACKGROUND_LIGHT);
        let axis_color = Color::rgb8(0x00, 0x00, 0x00);
        let stock_color = Color::rgb8(0xd8, 0x85, 0x43);
        let board_color = Color::rgb8(0xfe, 0xbf, 0x10);
        let traces_color = Color::rgb8(0xc8, 0x75, 0x33);
        let cut_color = Color::rgb8(0x00, 0x00, 0x00);
        let canvas_size = Point::from(ctx.size());

        ctx.fill(
            Rect::new(0.0, 0.0, canvas_size.x, canvas_size.y),
            &background_color,
        );

        ctx.clip(Rect::new(0.0, 0.0, canvas_size.x, canvas_size.y));

        //let (min_point, max_point) = job.bounds();
        let min_point = state.settings.stock_offset;

        let max_point = min_point + state.settings.stock_size;

        let size = max_point - min_point;

        if size.x > 0.0 && size.y > 0.0 {
            let mid_point = (min_point + max_point) * 0.5f64;

            let scale = f64::min(canvas_size.x / size.x, canvas_size.y / size.y);

            ctx.transform(Affine::translate(self.offset));
            ctx.transform(Affine::translate(canvas_size / 2.0));
            ctx.transform(Affine::scale(scale * 0.9));
            ctx.transform(Affine::FLIP_Y);
            ctx.transform(Affine::scale(self.scale));
            ctx.transform(Affine::translate(-mid_point));

            ctx.fill(
                &[
                    PathEl::MoveTo(min_point.into()),
                    PathEl::LineTo(
                        Point {
                            x: min_point.x,
                            y: max_point.y,
                        }
                        .into(),
                    ),
                    PathEl::LineTo(max_point.into()),
                    PathEl::LineTo(
                        Point {
                            x: max_point.x,
                            y: min_point.y,
                        }
                        .into(),
                    ),
                    PathEl::LineTo(min_point.into()),
                ][..],
                &stock_color,
            );

            for op in job.ops.values() {
                let mut geometry = op.geometry.geometry.clone() + min_point + job.origin();

                if op.side == OpSide::Bottom {
                    geometry.mirror(state.settings.flip_point_1, state.settings.flip_point_2);
                }

                match op.op_type {
                    OpType::Clear => {
                        filled_multipolygon(ctx, &geometry, &board_color, &background_color)
                    }
                    OpType::Cut(_) => stroked_multipolygon(
                        ctx,
                        &geometry.into_boundary(),
                        &cut_color,
                        false,
                        false,
                    ),
                }

                for avoid in op.avoids.iter() {
                    let mut avoid = avoid.geometry.clone() + min_point + job.origin();

                    if op.side == OpSide::Bottom {
                        avoid.mirror(state.settings.flip_point_1, state.settings.flip_point_2);
                    }

                    filled_multipolygon(ctx, &avoid, &traces_color, &board_color);
                }

                for (tool_key, paths) in op.toolpaths.iter() {
                    if let (Some(tool), Some(paths)) = (state.tools.tools.get(tool_key), paths) {
                        let (r, g, b) = tool.color;
                        let mut paths = paths.clone() + min_point + job.origin();

                        if op.side == OpSide::Bottom {
                            paths.mirror(state.settings.flip_point_1, state.settings.flip_point_2);
                        }

                        // Doing this offset in paint causes slowdowns
                        //let cut = paths.clone().offset(tool.diameter / 2.0);
                        // filled_multipolygon(
                        //     ctx,
                        //     &cut,
                        //     &Color::rgba8(r, g, b, 127),
                        //     &background_color,
                        // );

                        stroked_multipolygon(ctx, &paths, &Color::rgb(r, g, b), true, true);
                    }
                }
            }

            ctx.stroke(
                &[
                    PathEl::MoveTo(state.settings.flip_point_1.into()),
                    PathEl::LineTo(state.settings.flip_point_2.into()),
                ][..],
                &axis_color,
                LINE_THICKNESS * 3.0,
            );

            ctx.stroke(
                &[
                    PathEl::MoveTo(Point { x: 10.0, y: 0.0 }.into()),
                    PathEl::LineTo(Point { x: 0.0, y: 0.0 }.into()),
                    PathEl::LineTo(Point { x: 0.0, y: 10.0 }.into()),
                ][..],
                &axis_color,
                LINE_THICKNESS * 3.0,
            );
        }
    }
}

pub struct MultiPolygonPreview {
    aspect_ratio: f64,
}

impl MultiPolygonPreview {
    pub fn new() -> MultiPolygonPreview {
        MultiPolygonPreview { aspect_ratio: 1.0 }
    }
}

impl Widget<MultiPolygon> for MultiPolygonPreview {
    fn event(
        &mut self,
        _ctx: &mut EventCtx,
        _event: &Event,
        multipolygon: &mut MultiPolygon,
        _env: &Env,
    ) {
        let (min_point, max_point) = multipolygon.bounds();
        let size = max_point - min_point;
        self.aspect_ratio = size.x / size.y;
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        multipolygon: &MultiPolygon,
        _env: &Env,
    ) {
        let (min_point, max_point) = multipolygon.bounds();
        let size = max_point - min_point;
        self.aspect_ratio = size.x / size.y;
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        old_multipolygon: &MultiPolygon,
        multipolygon: &MultiPolygon,
        _env: &Env,
    ) {
        if !old_multipolygon.same(multipolygon) {
            let (min_point, max_point) = multipolygon.bounds();
            let size = max_point - min_point;
            self.aspect_ratio = size.x / size.y;
            ctx.request_paint();
        }
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _mutlipolygon: &MultiPolygon,
        _env: &Env,
    ) -> Size {
        let max = bc.max();
        let max_ratio = max.width / max.height;
        if max_ratio < self.aspect_ratio {
            Size::new(max.width, max.width / self.aspect_ratio)
        } else {
            Size::new(max.height * self.aspect_ratio, max.height)
        }
    }

    fn paint(&mut self, ctx: &mut PaintCtx, multipolygon: &MultiPolygon, env: &Env) {
        let background_color = env.get(theme::BACKGROUND_LIGHT);
        let board_color = Color::rgb8(0xfe, 0xbf, 0x10);
        let canvas_size = Point::from(ctx.size());

        ctx.fill(
            Rect::new(0.0, 0.0, canvas_size.x, canvas_size.y),
            &background_color,
        );

        ctx.clip(Rect::new(0.0, 0.0, canvas_size.x, canvas_size.y));

        let (min_point, max_point) = multipolygon.bounds();
        let size = max_point - min_point;

        if size.x > 0.0 && size.y > 0.0 {
            let mid_point = (min_point + max_point) * 0.5f64;

            let scale = f64::min(canvas_size.x / size.x, canvas_size.y / size.y);

            ctx.transform(Affine::translate(canvas_size / 2.0));
            ctx.transform(Affine::scale(scale * 0.9));
            ctx.transform(Affine::FLIP_Y);
            ctx.transform(Affine::translate(-mid_point));

            filled_multipolygon(ctx, multipolygon, &board_color, &background_color);
        }
    }
}

fn filled_multipolygon(
    ctx: &mut PaintCtx,
    geometry: &MultiPolygon,
    color: &Color,
    hole_color: &Color,
) {
    for polygon in geometry.polygons.iter() {
        if let Some(&first_point) = polygon.exterior.points.head() {
            let mut shape = Vec::new();
            shape.push(PathEl::MoveTo(first_point.into()));

            for point in polygon.exterior.points.iter().skip(1) {
                shape.push(PathEl::LineTo(point.into()))
            }
            ctx.fill(&shape[..], color);

            for interior in polygon.interiors.iter() {
                let points: Vector<_> = interior.points.iter().rev().collect();
                if let Some(&&first_interior_point) = points.head() {
                    let mut shape = Vec::new();
                    shape.push(PathEl::MoveTo(first_interior_point.into()));

                    for &point in points.iter().skip(1) {
                        shape.push(PathEl::LineTo(point.into()))
                    }
                    ctx.fill(&shape[..], hole_color);
                }
            }
        }
    }
}

fn stroked_multipolygon(
    ctx: &mut PaintCtx,
    paths: &MultiLineString,
    color: &Color,
    draw_arrow: bool,
    connect_lines: bool,
) {
    let mut last_point: Option<Point> = None;
    for linestring in paths.linestrings.iter() {
        if let Some(&first_point) = linestring.points.head() {
            let mut shape = Vec::new();
            shape.push(PathEl::MoveTo(first_point.into()));

            for &point in linestring.points.iter().skip(1) {
                shape.push(PathEl::LineTo(point.into()))
            }

            ctx.stroke_styled(
                &shape[..],
                color,
                LINE_THICKNESS,
                &StrokeStyle {
                    line_join: Some(LineJoin::Round),
                    line_cap: Some(LineCap::Round),
                    dash: None,
                    miter_limit: None,
                },
            );
        }

        if draw_arrow {
            if let (Some(&second_last), Some(&last)) = (
                linestring.points.iter().nth_back(1),
                linestring.points.back(),
            ) {
                arrow(
                    ctx,
                    second_last,
                    last,
                    &color.clone().with_alpha(0.5),
                    LINE_THICKNESS,
                );
            }
        }

        if connect_lines {
            if let (Some(last_point), Some(first_point)) = (&last_point, linestring.points.front())
            {
                ctx.stroke_styled(
                    &[
                        PathEl::MoveTo(last_point.into()),
                        PathEl::LineTo(first_point.into()),
                    ][..],
                    &color.clone().with_alpha(0.5),
                    LINE_THICKNESS,
                    &StrokeStyle {
                        line_join: Some(LineJoin::Round),
                        line_cap: Some(LineCap::Round),
                        dash: Some((vec![DASH_LENGTH, DASH_LENGTH], 0.0)),
                        miter_limit: None,
                    },
                )
            }
        }

        last_point = linestring.points.back().cloned();
    }
}

fn arrow(ctx: &mut PaintCtx, from: Point, to: Point, color: &Color, thickness: f64) {
    let direction_point = to - from;
    let direction = f64::atan2(direction_point.y, direction_point.x);

    let direction_left = direction + 3.0 * FRAC_PI_4;

    let left_point = Point {
        x: to.x + ARROW_LENGTH * f64::cos(direction_left),
        y: to.y + ARROW_LENGTH * f64::sin(direction_left),
    };

    let direction_right = direction - 3.0 * FRAC_PI_4;

    let right_point = Point {
        x: to.x + ARROW_LENGTH * f64::cos(direction_right),
        y: to.y + ARROW_LENGTH * f64::sin(direction_right),
    };

    ctx.stroke_styled(
        &[
            PathEl::MoveTo(left_point.into()),
            PathEl::LineTo(to.into()),
            PathEl::LineTo(right_point.into()),
        ][..],
        color,
        thickness,
        &StrokeStyle {
            line_join: Some(LineJoin::Round),
            line_cap: Some(LineCap::Round),
            dash: None,
            miter_limit: None,
        },
    );
}

use crate::cam::geometry::{LineString, MultiLineString, MultiPolygon, Point, Polygon};
use crate::cam::job::{
    CutType, GeometryTarget, Job, LoadGeometry, LoadedGeometry, LoadedGeometrySelect,
    LoadingGeometry, Op, OpDepth, OpGeometry, OpKey, OpSide, OpType, OutlinedFilledGeometry,
};
use crate::cam::{RequestType, TO_CAM};
use crate::settings::{ToolKey, ToolTable};
use crate::ui::canvas::MultiPolygonPreview;
use crate::ui::lenses::{EmptyLens, KeyedLens};
use crate::ui::shared_list::Keyed;
use crate::ui::widgets::{Select, ToolSelect, ToolTarget, SELECTED_TOOL, SELECT_TOOL};
use crate::ui::{point_setting_widget, remove_button};
use crate::{CamState, Selection};
use druid::widget::{
    Button, Checkbox, Controller, ControllerHost, CrossAxisAlignment, Flex, Label, List, Radio,
    Scroll,
};
use druid::{
    lens, Command, Data, Env, Event, EventCtx, FileDialogOptions, FileSpec, FontDescriptor,
    FontFamily, Lens, LensExt, Selector, UnitPoint, Widget, WidgetExt,
};
use match_macro::match_widget;
use slotmap::SecondaryMap;
use std::path::PathBuf;
use std::sync::Arc;

use super::shared_list::SharedListItem;
use super::widgets::Collapse;
use crate::render_3d::TO_RENDER3D;

pub const INPUT_FILE: FileSpec = FileSpec {
    name: "Geometry",
    extensions: &["gbr", "drl", "dxf", "svg"],
};

pub const GCODE: FileSpec = FileSpec {
    name: "GCode",
    extensions: &["gcode", "nc", "ngc"],
};

fn point_widget() -> impl Widget<Point> {
    Label::new(|point: &Point, _env: &Env| format!("{:07.3}, {:07.3}", point.x, point.y))
        .with_font(FontDescriptor::new(FontFamily::MONOSPACE))
        .align_left()
}

fn linestring_widget() -> impl Widget<LineString> {
    List::new(point_widget).lens(LineString::points)
}

fn multilinestring_widget() -> impl Widget<MultiLineString> {
    List::new(|| Collapse::new(Label::new("Path"), linestring_widget(), true))
        .lens(MultiLineString::linestrings)
}

fn polygon_widget() -> impl Widget<Polygon> {
    Flex::column()
        .with_child(Collapse::new(
            Label::new("Exterior"),
            linestring_widget().lens(Polygon::exterior),
            true,
        ))
        .with_child(Collapse::new(
            Label::new("Interiors"),
            Collapse::new(
                Label::new("Path"),
                List::new(linestring_widget).lens(Polygon::interiors),
                true,
            ),
            true,
        ))
        .cross_axis_alignment(CrossAxisAlignment::Start)
}

fn multipolygon_widget() -> impl Widget<MultiPolygon> {
    List::new(|| Collapse::new(Label::new("Polygon"), polygon_widget(), true))
        .lens(MultiPolygon::polygons)
}

fn multipolygon_widget_option() -> impl Widget<Option<MultiPolygon>> {
    match_widget! { Option<MultiPolygon>,
        Some(MultiPolygon) => multipolygon_widget(),
        None => Label::new("No Geometry")
    }
}

type ToolpathItem = SharedListItem<ToolTable, ToolKey, Option<MultiLineString>>;

fn toolpath_widget() -> impl Widget<Option<MultiLineString>> {
    match_widget! { Option<MultiLineString>,
        Some(MultiLineString) => multilinestring_widget(),
        None => Label::new("No Toolpaths")
    }
}

fn toolpaths_widget() -> impl Widget<ToolpathItem> {
    Collapse::new(
        Flex::row()
            .with_flex_child(
                Label::new(|item: &ToolpathItem, _env: &Env| {
                    if let Some(tool) = item.shared.tools.get(item.key) {
                        tool.name.clone()
                    } else {
                        "Unknown Tool".to_string()
                    }
                })
                .expand_width(),
                1.0,
            )
            .with_child(remove_button()),
        toolpath_widget().lens(lens!(ToolpathItem, value)),
        true,
    )
}

fn tools_widget<C>() -> impl Widget<(ToolTable, ToolTarget, Arc<C>)>
where
    C: 'static + Keyed<ToolKey, Option<MultiLineString>> + Clone,
{
    Collapse::new(
        Flex::row()
            .with_flex_child(Label::new("Tools").expand_width(), 1.0)
            .with_child(
                Button::new("+")
                    .on_click(
                        move |ctx: &mut EventCtx, target: &mut ToolTarget, _env: &Env| {
                            ctx.submit_command(SELECT_TOOL.with(target.clone()))
                        },
                    )
                    .lens(lens!((ToolTable, ToolTarget, Arc<C>), 1)),
            ),
        List::new(toolpaths_widget).lens(KeyedLens::shared(
            lens!((ToolTable, ToolTarget, Arc<C>), 0),
            lens!((ToolTable, ToolTarget, Arc<C>), 2),
        )),
        false,
    )
}

fn op_geometry_widget() -> impl Widget<OpGeometry> {
    Collapse::new(
        Flex::row()
            .with_child(Label::new(|g: &OpGeometry, _env: &Env| {
                g.filename
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string()
            }))
            .with_flex_spacer(1.0)
            .with_child(Label::new(|g: &OpGeometry, _env: &Env| {
                let (min_point, max_point) = g.geometry.bounds();
                let size = max_point - min_point;
                format!("{:.2}×{:.2}", size.x, size.y)
            }))
            .expand_width(),
        multipolygon_widget().lens(lens!(OpGeometry, geometry)),
        true,
    )
}

type AvoidItem = SharedListItem<(), usize, OpGeometry>;

fn avoid_widget() -> impl Widget<AvoidItem> {
    Flex::row()
        .with_flex_child(op_geometry_widget().lens(lens!(AvoidItem, value)), 1.0)
        .with_child(remove_button().align_vertical(UnitPoint::TOP))
}

fn avoids_widget() -> impl Widget<OpListItem> {
    Collapse::new(
        Flex::row()
            .with_flex_child(Label::new("Avoids").expand_width(), 1.0)
            .with_child(Button::new("+").on_click(
                |ctx: &mut EventCtx, i: &mut OpListItem, _env: &Env| {
                    ctx.submit_command(Command::new(ADD_AVOID, i.key, ctx.window_id()))
                },
            )),
        List::new(avoid_widget).lens(KeyedLens::shared(
            EmptyLens,
            lens!(OpListItem, value).then(lens!(Op, avoids)),
        )),
        false,
    )
}

fn op_side_widget() -> impl Widget<OpSide> {
    Collapse::new(
        Label::new(|op_side: &OpSide, _env: &Env| match op_side {
            OpSide::Top => "Op Side: Top".to_string(),
            OpSide::Bottom => "Op Side: Bottom".to_string(),
        }),
        Flex::column()
            .with_child(Radio::new("Top", OpSide::Top).align_left())
            .with_child(Radio::new("Bottom", OpSide::Bottom).align_left()),
        false,
    )
}

fn op_type_widget() -> impl Widget<OpType> {
    Collapse::new(
        Label::new(|op_type: &OpType, _env: &Env| match op_type {
            OpType::Clear => "Op Type: Clear".to_string(),
            OpType::Cut(CutType::Inside) => "Op Type: Cut Inside".to_string(),
            OpType::Cut(CutType::On) => "Op Type: Cut On".to_string(),
            OpType::Cut(CutType::Outside) => "Op Type: Cut Outside".to_string(),
        }),
        Flex::column()
            .with_child(Radio::new("Clear", OpType::Clear).align_left())
            .with_child(Radio::new("Cut Inside", OpType::Cut(CutType::Inside)).align_left())
            .with_child(Radio::new("Cut On", OpType::Cut(CutType::On)).align_left())
            .with_child(Radio::new("Cut Outside", OpType::Cut(CutType::Outside)).align_left()),
        false,
    )
}

fn op_depth_widget() -> impl Widget<OpDepth> {
    Collapse::new(
        Label::new(|depth: &OpDepth, _env: &Env| match depth {
            OpDepth::Surface => "Depth: Surface".to_string(),
            OpDepth::Through => "Depth: Through".to_string(),
        }),
        Flex::column()
            .with_child(Radio::new("Surface", OpDepth::Surface).align_left())
            .with_child(Radio::new("Through", OpDepth::Through).align_left()),
        false,
    )
}

type OpListItem = SharedListItem<CamState, OpKey, Op>;

fn operation_widget() -> impl Widget<OpListItem> {
    Collapse::new(
        Flex::row()
            .with_flex_child(
                Select::new(
                    |item: &OpListItem, _env: &Env| {
                        if let Selection::Op(sel_op) = item.shared.selection {
                            sel_op == item.key
                        } else {
                            false
                        }
                    },
                    Label::new(|item: &OpListItem, _env: &Env| {
                        format!(
                            "{}",
                            item.value
                                .geometry
                                .filename
                                .file_name()
                                .unwrap()
                                .to_string_lossy()
                        )
                    }),
                )
                .on_click(|_ctx: &mut EventCtx, item: &mut OpListItem, _env: &Env| {
                    if item.shared.selection == Selection::Op(item.key) {
                        item.shared.selection = Selection::None;
                    } else {
                        item.shared.selection = Selection::Op(item.key);
                    }
                })
                .expand_width(),
                1.0,
            )
            .with_child(cut_button(|item: &OpListItem| RequestType::Cut(item.key)))
            .with_child(remove_button()),
        Flex::column()
            .with_child(
                op_side_widget()
                    .align_left()
                    .lens(lens!(OpListItem, value).then(lens!(Op, side))),
            )
            .with_child(
                op_type_widget()
                    .align_left()
                    .lens(lens!(OpListItem, value).then(lens!(Op, op_type))),
            )
            .with_child(
                op_depth_widget()
                    .align_left()
                    .lens(lens!(OpListItem, value).then(lens!(Op, depth))),
            )
            .with_child(
                Checkbox::new("Allow outside cuts")
                    .align_left()
                    .lens(lens!(OpListItem, value).then(lens!(Op, allow_outside_cuts))),
            )
            // .with_child(
            //     op_geometry_widget_option().lens(IndexKeyedLens::new(
            //         lens!(OpListItem, value).then(lens!(Op, geometry)),
            //         lens!(OpListItem, shared)
            //             .then(lens!(CamState, job))
            //             .then(lens!(Job, geometries)),
            //     )),
            // )
            .with_child(tools_widget().lens(lens::Identity.map(
                |item: &OpListItem| {
                    (
                        item.shared.tools.clone(),
                        ToolTarget::Op(item.key.clone()),
                        item.value.toolpaths.clone(),
                    )
                },
                |item: &mut OpListItem,
                 (tooltable, target, toolpaths): (
                    ToolTable,
                    ToolTarget,
                    Arc<SecondaryMap<ToolKey, Option<MultiLineString>>>,
                )| {
                    item.shared.tools = tooltable;
                    let ToolTarget::Op(op_key) = target;
                    item.key = op_key;
                    item.value.toolpaths = toolpaths;
                },
            )))
            .with_child(avoids_widget()),
        false,
    )
}

fn cut_button<T: Data, F: 'static + Fn(&T) -> RequestType>(f: F) -> impl Widget<T> {
    Button::new("Cut").on_click(move |ctx: &mut EventCtx, item: &mut T, _env: &Env| {
        ctx.submit_command(Command::new(TO_CAM, f(item), ctx.window_id()))
    })
}

fn operations_widget() -> impl Widget<CamState> {
    Flex::column()
        .with_child(Label::new("Job"))
        .with_child(point_setting_widget("Offset").lens(CamState::job.then(Job::offset)))
        .with_flex_child(
            Scroll::new(List::new(operation_widget).lens(KeyedLens::shared(
                lens::Identity,
                CamState::job.then(Job::ops),
            )))
            .vertical()
            .expand(),
            1.0,
        )
        .with_child(ToolSelect::new().lens(CamState::tools))
        .with_child(
            Flex::row()
                .with_flex_child(
                    Button::new("Add Operation")
                        .on_click(|ctx: &mut EventCtx, _state: &mut CamState, _env: &Env| {
                            ctx.submit_command(Command::new(ADD_OPERATION, (), ctx.window_id()))
                        })
                        .expand_width(),
                    1.0,
                )
                .with_flex_child(
                    Button::new("Export Gcode")
                        .on_click(|ctx: &mut EventCtx, _job: &mut Job, _env: &Env| {
                            ctx.submit_command(Command::new(
                                druid::commands::SHOW_SAVE_PANEL,
                                FileDialogOptions::new().allowed_types(vec![GCODE]),
                                ctx.window_id(),
                            ))
                        })
                        .expand_width()
                        .lens(CamState::job),
                    1.0,
                ),
        )
}

fn loading_geometry_widget() -> impl Widget<LoadingGeometry> {
    Flex::column()
        .with_child(Label::new(
            |loading_geometry: &LoadingGeometry, _env: &Env| {
                loading_geometry
                    .filename
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string()
            },
        ))
        .with_child(Label::new("Loading..."))
}

fn loaded_geometry_select_outlined_filled_widget() -> impl Widget<OutlinedFilledGeometry> {
    Flex::column()
        .with_child(Radio::new("Filled", false).lens(OutlinedFilledGeometry::is_outlined))
        .with_child(
            MultiPolygonPreview::new()
                .lens(OutlinedFilledGeometry::filled)
                .fix_height(100.0),
        )
        .with_child(Radio::new("Outlined", true).lens(OutlinedFilledGeometry::is_outlined))
        .with_child(
            MultiPolygonPreview::new()
                .lens(OutlinedFilledGeometry::outlined)
                .fix_height(100.0),
        )
}

fn loaded_geometry_select_filled_widget() -> impl Widget<MultiPolygon> {
    MultiPolygonPreview::new()
}

fn loaded_geometry_select_widget() -> impl Widget<LoadedGeometrySelect> {
    match_widget! { LoadedGeometrySelect,
        LoadedGeometrySelect::OutlinedFilled(OutlinedFilledGeometry) => loaded_geometry_select_outlined_filled_widget(),
        LoadedGeometrySelect::Filled(MultiPolygon) => loaded_geometry_select_filled_widget(),
    }
}

fn loaded_geometry_widget() -> impl Widget<LoadedGeometry> {
    Flex::column()
        .with_child(Label::new(
            |loaded_geometry: &LoadedGeometry, _env: &Env| {
                loaded_geometry
                    .filename
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string()
            },
        ))
        .with_child(loaded_geometry_select_widget().lens(LoadedGeometry::geometry))
        .with_child(
            Flex::row()
                .with_child(Button::new("Ok").on_click(
                    |ctx: &mut EventCtx, _loaded_geometry: &mut LoadedGeometry, _env: &Env| {
                        ctx.submit_command(Command::new(CONFIRM_GEOMETRY, (), ctx.window_id()))
                    },
                ))
                .with_child(Button::new("Cancel").on_click(
                    |ctx: &mut EventCtx, _loaded_geometry: &mut LoadedGeometry, _env: &Env| {
                        ctx.submit_command(Command::new(CANCEL_GEOMETRY, (), ctx.window_id()))
                    },
                )),
        )
}

pub fn load_geometry_widget() -> impl Widget<LoadGeometry> {
    match_widget! { LoadGeometry,
        LoadGeometry::None => Flex::row(),
        LoadGeometry::Loading(LoadingGeometry) => loading_geometry_widget(),
        LoadGeometry::Loaded(LoadedGeometry) => loaded_geometry_widget(),
    }
}

pub fn job_widget() -> impl Widget<CamState> {
    ControllerHost::new(
        Flex::column()
            .with_flex_child(operations_widget(), 1.0)
            .with_child(load_geometry_widget().lens(CamState::loading_geometry)),
        JobController,
    )
}

#[derive(Clone, Data)]
pub enum JobOpenFile {
    LoadOp,
    LoadAvoid(OpKey),
}

fn load_geometry(
    state: &mut CamState,
    ctx: &mut EventCtx,
    filename: PathBuf,
    target: GeometryTarget,
) {
    state.loading_geometry = LoadGeometry::Loading(LoadingGeometry {
        filename: Arc::new(filename.clone()),
        target,
    });

    ctx.submit_command(Command::new(
        TO_CAM,
        RequestType::Load(filename),
        ctx.window_id(),
    ));
}

pub const ADD_OPERATION: Selector<()> = Selector::new("tim.pycut.add-operation");
pub const ADD_AVOID: Selector<OpKey> = Selector::new("tim.pycut.add-avoid");
pub const CANCEL_GEOMETRY: Selector<()> = Selector::new("tim.pycut.cancel-geometry");
pub const CONFIRM_GEOMETRY: Selector<()> = Selector::new("tim.pycut.add-geometry");

struct JobController;

impl<W: Widget<CamState>> Controller<CamState, W> for JobController {
    fn event(
        &mut self,
        child: &mut W,
        ctx: &mut EventCtx,
        event: &Event,
        state: &mut CamState,
        env: &Env,
    ) {
        if let Event::Command(c) = event {
            if let Some(()) = c.get(ADD_OPERATION) {
                state.open_file = Some(JobOpenFile::LoadOp);
                ctx.submit_command(Command::new(
                    druid::commands::SHOW_OPEN_PANEL,
                    FileDialogOptions::new().allowed_types(vec![INPUT_FILE]),
                    ctx.window_id(),
                ))
            } else if let Some(op_key) = c.get(ADD_AVOID) {
                state.open_file = Some(JobOpenFile::LoadAvoid(op_key.clone()));
                ctx.submit_command(Command::new(
                    druid::commands::SHOW_OPEN_PANEL,
                    FileDialogOptions::new().allowed_types(vec![INPUT_FILE]),
                    ctx.window_id(),
                ))
            } else if let Some(()) = c.get(CANCEL_GEOMETRY) {
                state.loading_geometry = LoadGeometry::None;
            } else if let Some(()) = c.get(CONFIRM_GEOMETRY) {
                if let LoadGeometry::Loaded(loaded_geometry) = state.loading_geometry.clone() {
                    CamState::job.then(Job::ops).with_mut(state, |ops| {
                        let mut new_ops = ops.as_ref().to_owned();
                        match loaded_geometry.target {
                            GeometryTarget::Op => {
                                new_ops.insert(Op {
                                    name: loaded_geometry
                                        .filename
                                        .file_name()
                                        .unwrap()
                                        .to_string_lossy()
                                        .to_string(),
                                    geometry: loaded_geometry.into(),
                                    side: OpSide::Top,
                                    op_type: OpType::Clear,
                                    depth: OpDepth::Surface,
                                    allow_outside_cuts: true,
                                    toolpaths: Arc::new(SecondaryMap::new()),
                                    avoids: Arc::new(Vec::new()),
                                });
                            }
                            GeometryTarget::Avoid(op_key) => {
                                if let Some(op) = new_ops.get_mut(op_key) {
                                    let mut new_avoids = op.avoids.as_ref().to_owned();
                                    new_avoids.push(loaded_geometry.into());
                                    op.avoids = Arc::new(new_avoids);
                                }
                            }
                        }
                        *ops = Arc::new(new_ops);
                    })
                }
                ctx.submit_command(TO_RENDER3D);
                state.loading_geometry = LoadGeometry::None;
            } else if let Some(file_info) = c.get(druid::commands::OPEN_FILE) {
                match state.open_file {
                    Some(JobOpenFile::LoadOp) => {
                        load_geometry(
                            state,
                            ctx,
                            file_info.path().to_path_buf(),
                            GeometryTarget::Op,
                        );
                    }
                    Some(JobOpenFile::LoadAvoid(op_key)) => {
                        load_geometry(
                            state,
                            ctx,
                            file_info.path().to_path_buf(),
                            GeometryTarget::Avoid(op_key),
                        );
                    }
                    None => {}
                }
                state.open_file = None;
            } else if let Some(file_info) = c.get(druid::commands::SAVE_FILE_AS) {
                ctx.submit_command(Command::new(
                    TO_CAM,
                    RequestType::ExportGcode(file_info.path().to_owned()),
                    ctx.window_id(),
                ));
            } else if let Some((target, tool_key)) = c.get(SELECTED_TOOL) {
                match target {
                    ToolTarget::Op(op_key) => {
                        CamState::job.then(Job::ops).with_mut(state, |ops| {
                            let mut new_ops = ops.as_ref().to_owned();

                            if let Some(mut op) = new_ops.get_mut(op_key.clone()) {
                                let mut new_toolpaths = op.toolpaths.as_ref().clone();
                                new_toolpaths.insert(*tool_key, None);
                                op.toolpaths = Arc::new(new_toolpaths);
                            }

                            *ops = Arc::new(new_ops);
                        });
                    }
                }
            }
        }

        child.event(ctx, event, state, env)
    }
}

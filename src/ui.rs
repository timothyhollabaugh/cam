mod canvas;
pub(crate) mod job;
pub mod lenses;
mod settings;
pub mod shared_list;
mod widgets;

use self::lenses::KeyedLens;
use self::shared_list::SharedListItem;
use self::widgets::Collapse;
use crate::cam::geometry::Point;
use crate::cam::CamError;
use crate::settings::{
    save, Settings, ToolKey, ToolSettings, ToolTable, SETTINGS_FILENAME, TOOLS_FILENAME,
};
use crate::ui::canvas::CamCanvas;
use crate::ui::job::job_widget;
use crate::{CamState, Progress, Status};
use druid::text::format::{Formatter, Validation, ValidationError};
use druid::text::Selection;
use druid::widget::{
    Button, CrossAxisAlignment, Flex, Label, List, MainAxisAlignment, ProgressBar, Scroll, TextBox,
    ViewSwitcher,
};
use druid::{lens, Color, Data, Env, EventCtx, TextAlignment, Widget, WidgetExt};
use match_macro::match_widget;
use std::sync::Arc;

fn remove_button<S: Data, K: Data, T: Data>() -> impl Widget<SharedListItem<S, K, T>> {
    Button::new("-")
        .on_click(|_, item: &mut SharedListItem<S, K, T>, _| item.list_ops.remove(item.key.clone()))
}

fn point_setting_widget(name: &str) -> impl Widget<Point> {
    Collapse::new(
        Flex::row()
            .with_child(Label::new(name))
            .with_flex_spacer(1.0)
            .with_child(Label::new(|p: &Point, _env: &Env| {
                format!("{:.2}, {:.2}", p.x, p.y)
            }))
            .expand_width(),
        Flex::column()
            .with_child(unit_setting_widget("X", "mm", 2).lens(Point::x))
            .with_child(unit_setting_widget("Y", "mm", 2).lens(Point::y)),
        true,
    )
}

fn string_setting_widget(name: &str) -> impl Widget<String> {
    Flex::row()
        .with_flex_child(Label::new(name).expand_width(), 1.0)
        .with_child(TextBox::new().fix_width(150.0))
        .expand_width()
    //.padding((10.0, 0.0, 0.0, 0.0))
}

pub struct UnitFormatter {
    pub unit: String,
    pub decimals: usize,
}

impl Formatter<f64> for UnitFormatter {
    fn format(&self, value: &f64) -> String {
        format!(
            "{value:.decimals$} {unit}",
            value = value,
            decimals = self.decimals,
            unit = self.unit
        )
    }

    fn format_for_editing(&self, value: &f64) -> String {
        format!(
            "{value:.decimals$}",
            value = value,
            decimals = self.decimals,
        )
    }

    fn validate_partial_input(&self, input: &str, _sel: &Selection) -> Validation {
        let with_trailing_zero = format!("{}0", input.trim());
        match with_trailing_zero.parse::<f64>() {
            Ok(_) => Validation::success(),
            Err(e) => Validation::failure(e),
        }
    }

    fn value(&self, input: &str) -> Result<f64, ValidationError> {
        input.parse().map_err(|e| ValidationError::new(e))
    }
}

fn unit_setting_widget(name: &str, unit: &str, decimals: usize) -> impl Widget<f64> {
    let formatter = UnitFormatter {
        unit: unit.to_string(),
        decimals,
    };

    Flex::row()
        .with_flex_child(Label::new(name).expand_width(), 1.0)
        .with_child(
            TextBox::new()
                .with_text_alignment(TextAlignment::End)
                .with_formatter(formatter)
                .fix_width(100.0),
        )
        .expand_width()
}

fn color_setting_widget(name: &str) -> impl Widget<(f64, f64, f64)> {
    Flex::row()
        .with_flex_child(Label::new(name).expand_width(), 1.0)
        .with_child(
            TextBox::new()
                .with_formatter(UnitFormatter {
                    decimals: 1,
                    unit: "r".to_string(),
                })
                .fix_width(40.0)
                .lens(lens!((f64, f64, f64), 0)),
        )
        .with_child(
            TextBox::new()
                .with_formatter(UnitFormatter {
                    decimals: 1,
                    unit: "g".to_string(),
                })
                .fix_width(40.0)
                .lens(lens!((f64, f64, f64), 1)),
        )
        .with_child(
            TextBox::new()
                .with_formatter(UnitFormatter {
                    decimals: 1,
                    unit: "b".to_string(),
                })
                .fix_width(40.0)
                .lens(lens!((f64, f64, f64), 2)),
        )
        .expand_width()
    //.padding((10.0, 0.0, 0.0, 0.0))
}

fn tool_settings_widget() -> impl Widget<SharedListItem<(), ToolKey, ToolSettings>> {
    Collapse::new(
        Flex::row()
            .with_flex_child(
                Label::new(
                    |item: &SharedListItem<(), ToolKey, ToolSettings>, _env: &Env| {
                        item.value.name.clone()
                    },
                )
                .expand_width(),
                1.0,
            )
            .with_child(remove_button())
            .padding((5.0, 0.0, 0.0, 0.0)),
        Flex::column()
            .with_child(string_setting_widget("Name").lens(ToolSettings::name))
            .with_child(unit_setting_widget("Diameter", "mm", 2).lens(ToolSettings::diameter))
            .with_child(unit_setting_widget("Overcut", "mm", 2).lens(ToolSettings::overcut))
            .with_child(
                unit_setting_widget("Min cut area", "mm²", 4).lens(ToolSettings::min_cut_area),
            )
            .with_child(unit_setting_widget("Safe Z", "mm", 2).lens(ToolSettings::safe_z))
            .with_child(unit_setting_widget("Cut Z", "mm", 3).lens(ToolSettings::cut_z))
            .with_child(unit_setting_widget("Z Feedrate", "mm", 2).lens(ToolSettings::z_feedrate))
            .with_child(unit_setting_widget("XY Feedrate", "mm", 2).lens(ToolSettings::xy_feedrate))
            .with_child(unit_setting_widget("Cut Depth", "mm", 3).lens(ToolSettings::cut_depth))
            .with_child(unit_setting_widget("Spindle", "rpm", 0).lens(ToolSettings::spindle_speed))
            .with_child(color_setting_widget("Color").lens(ToolSettings::color))
            .lens(lens!(SharedListItem<(), ToolKey, ToolSettings>, value)),
        true,
    )
    .expand_width()
}

fn tool_table_widget() -> impl Widget<ToolTable> {
    Flex::column()
        .with_child(Label::new("Tools"))
        .with_flex_child(
            Scroll::new(List::new(tool_settings_widget).lens(KeyedLens::new(ToolTable::tools)))
                .vertical()
                .expand_height(),
            1.0,
        )
        .with_child(Button::new("Add Tool").on_click(
            |_ctx: &mut EventCtx, table: &mut ToolTable, _env: &Env| {
                let mut new_tools = (*table.tools).clone();
                new_tools.insert(ToolSettings::default());
                table.tools = Arc::new(new_tools);
            },
        ))
}

fn settings_widget() -> impl Widget<Settings> {
    Flex::column()
        .with_child(Label::new("Settings"))
        .with_child(point_setting_widget("Loading").lens(Settings::loading))
        .with_child(unit_setting_widget("Clearance Z", "mm", 2).lens(Settings::clearance_z))
        .with_child(point_setting_widget("Stock Size").lens(Settings::stock_size))
        .with_child(unit_setting_widget("Stock Thickness", "mm", 2).lens(Settings::stock_thickness))
        .with_child(point_setting_widget("Stock Offset").lens(Settings::stock_offset))
        .with_child(unit_setting_widget("Stock Offset Z", "mm", 2).lens(Settings::stock_offset_z))
        .with_child(point_setting_widget("Flip Point 1").lens(Settings::flip_point_1))
        .with_child(point_setting_widget("Flip Point 2").lens(Settings::flip_point_2))
        .with_child(point_setting_widget("Toolchange Point").lens(Settings::toolchange))
        .with_child(
            unit_setting_widget("Toolchange Probe Start Z", "mm", 2)
                .lens(Settings::toolchange_probe_start_z),
        )
        .with_child(
            unit_setting_widget("Toolchange Probe End Z", "mm", 2)
                .lens(Settings::toolchange_probe_end_z),
        )
        .with_child(
            unit_setting_widget("Toolchange Probe Feedrate", "mm", 2)
                .lens(Settings::toolchange_probe_feedrate),
        )
        .with_child(
            unit_setting_widget("Toolchange Z Offset", "mm", 2).lens(Settings::toolchange_z_offset),
        )
}

pub fn progress_widget() -> impl Widget<Progress> {
    Flex::row()
        .with_flex_child(
            ProgressBar::new()
                .expand_width()
                .padding(5.0)
                .lens(Progress::progress),
            1.0,
        )
        .with_child(
            Label::new(|progress: &Progress, _env: &Env| {
                format!("{:.3}%", progress.progress * 100.0)
            })
            .align_left()
            .fix_width(100.0)
            .padding(5.0),
        )
        .with_child(
            Label::new(|progress: &Progress, _env: &Env| progress.msg.clone())
                .align_right()
                .fix_width(300.0)
                .padding(5.0),
        )
        .main_axis_alignment(MainAxisAlignment::SpaceAround)
}

pub fn error_widget() -> impl Widget<CamError> {
    Label::new(|error: &CamError, _env: &Env| match error {
        &CamError::NoGeometry => "No geometry".to_string(),
        &CamError::NoOutline => "No outline".to_string(),
        &CamError::InvalidTool => "Invalid tool".to_string(),
        &CamError::InvalidDrill => "Drill not found".to_string(),
        &CamError::InvalidOp => "Op not found".to_string(),
        &CamError::Wkt(_) => "Wkt error".to_string(),
        &CamError::WrongType => "Got the wrong kind of geometry".to_string(),
        &CamError::Dxf(ref e) => format!("DXF error: {}", e),
        &CamError::InvalidFileType => format!("Unsupported file type"),
    })
}

pub fn status_widget() -> impl Widget<Status> {
    //Label::new("Idle")
    match_widget! { Status,
        Status::Idle => Label::new("Idle"),
        Status::Progress(Progress) => progress_widget(),
        Status::Error(CamError) => error_widget(),
    }
}

pub fn ui_widget() -> impl Widget<CamState> {
    let settings = ViewSwitcher::new(
        |data: &CamState, _env| data.active_setting,
        move |tab, _data, _env| match tab {
            0 => Box::new(tool_table_widget().lens(CamState::tools)),
            1 => Box::new(settings_widget().lens(CamState::settings)),
            _ => panic!("Non-existent tab selected!"),
        },
    )
    .padding((0.0, 5.0))
    .expand_height();

    let settings_tab_buttons = Flex::row()
        .with_flex_child(
            Button::new("Tools")
                .on_click(|_ctx, data: &mut CamState, _env| data.active_setting = 0)
                .expand_width(),
            1.0,
        )
        .with_flex_child(
            Button::new("Settings")
                .on_click(|_ctx, data: &mut CamState, _env| data.active_setting = 1)
                .expand_width(),
            1.0,
        );

    let save_button =
        Button::new("Save").on_click(|_ctx: &mut EventCtx, state: &mut CamState, _env: &Env| {
            save(TOOLS_FILENAME, &state.tools);
            save(SETTINGS_FILENAME, &state.settings);
        });

    let settings_pane = Flex::column()
        .with_child(settings_tab_buttons)
        .with_flex_child(settings, 1.0)
        .with_child(save_button)
        .fix_width(250.0)
        .padding(5.0)
        .border(Color::BLACK, 1.0)
        .padding(5.0);

    let jobs_pane = job_widget()
        .fix_width(250.0)
        .expand_height()
        .padding(5.0)
        .border(Color::BLACK, 1.0)
        .padding(5.0);

    Flex::column()
        .with_flex_child(
            Flex::row()
                .with_child(jobs_pane)
                .with_flex_child(CamCanvas::new(), 1.0)
                .with_child(settings_pane)
                .cross_axis_alignment(CrossAxisAlignment::Start),
            1.0,
        )
        .with_child(status_widget().lens(CamState::status))
}

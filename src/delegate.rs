use crate::CamState;
use druid::{AppDelegate, Command, Data, DelegateCtx, Env, Handled, Target};

pub struct MultiDelegate<T: Data> {
    delegates: Vec<Box<dyn AppDelegate<T>>>,
}

impl<T: Data> MultiDelegate<T> {
    pub fn new() -> MultiDelegate<T> {
        MultiDelegate {
            delegates: Vec::new(),
        }
    }

    pub fn with_delegate<D: 'static + AppDelegate<T>>(mut self, delegate: D) -> MultiDelegate<T> {
        self.delegates.push(Box::new(delegate));
        self
    }
}

impl<T: Data> AppDelegate<T> for MultiDelegate<T> {
    // TODO: Forward the rest of the AppDelegate methods

    fn command(
        &mut self,
        ctx: &mut DelegateCtx,
        target: Target,
        cmd: &Command,
        data: &mut T,
        env: &Env,
    ) -> Handled {
        let mut handled = Handled::No;

        for delegate in self.delegates.iter_mut() {
            let h = delegate.command(ctx, target, cmd, data, env);
            if h == Handled::Yes {
                handled = Handled::Yes
            }
        }

        handled
    }
}

// Seems to be needed because of the deeply nested Vectors being sent across threads
#![recursion_limit = "256"]
#![type_length_limit = "1900530"]
#![feature(generic_associated_types)]
#![feature(iter_intersperse)]
#![feature(array_chunks)]

mod cam;
mod delegate;
mod render_3d;
mod settings;
mod test;
mod ui;

use crate::cam::job::LoadGeometry;
use crate::delegate::MultiDelegate;
use crate::render_3d::Render3dDelegate;
use cam::job::{Job, OpKey};
use cam::CamDelegate;
use cam::CamError;
use crossbeam::channel::unbounded;
use druid::{theme, AppLauncher, Color, Data, Env, Lens, Selector, WindowDesc};
use settings::Settings;
use settings::ToolTable;
use settings::{load, SETTINGS_FILENAME, TOOLS_FILENAME};
use std::path::PathBuf;
use std::thread;
use ui::job::JobOpenFile;
use ui::ui_widget;

pub const LOAD_GERBER: Selector<PathBuf> = Selector::new("tim.pycut.load-geber");

#[derive(Clone, Data, Lens)]
pub struct Progress {
    progress: f64,
    msg: String,
}

#[derive(Clone, Data)]
pub enum Status {
    Idle,
    Progress(Progress),
    Error(CamError),
}

#[derive(Clone, Data, PartialEq)]
pub enum Selection {
    Op(OpKey),
    None,
}

#[derive(Clone, Data, Lens)]
pub struct CamState {
    active_setting: usize,
    tools: ToolTable,
    settings: Settings,
    job: Job,
    loading_geometry: LoadGeometry,
    status: Status,
    selection: Selection,
    open_file: Option<JobOpenFile>,
}

fn main() {
    let main_window = WindowDesc::new(ui_widget)
        .title("Cam")
        .window_size((1200.0, 600.0));

    let tools = load(TOOLS_FILENAME);
    let settings = load(SETTINGS_FILENAME);

    let initial_state = CamState {
        active_setting: 0,
        job: Job::new(),
        loading_geometry: LoadGeometry::None,
        settings,
        tools,
        status: Status::Idle,
        selection: Selection::None,
        open_file: None,
    };

    let window_id = main_window.id;

    let (cam_tx, cam_rx) = unbounded();
    //let (render_3d_tx, render_3d_rx) = unbounded();

    let app = AppLauncher::with_window(main_window)
        .delegate(
            MultiDelegate::new().with_delegate(CamDelegate::new(cam_tx)), //.with_delegate(Render3dDelegate::new(render_3d_tx)),
        )
        .configure_env(|env: &mut Env, _state: &CamState| {
            env.set(
                theme::WINDOW_BACKGROUND_COLOR,
                Color::rgb8(0xf0, 0xf0, 0xf0),
            );
            env.set(theme::BACKGROUND_LIGHT, Color::rgb8(0xf9, 0xf9, 0xf9));
            env.set(theme::BACKGROUND_DARK, Color::rgb8(0xf9, 0xf9, 0xf9));
            env.set(theme::LABEL_COLOR, Color::rgb8(0x33, 0x33, 0x33));
            env.set(theme::BUTTON_DARK, Color::rgb8(0xf9, 0xf9, 0xf9));
            env.set(theme::BUTTON_LIGHT, Color::rgb8(0xf9, 0xf9, 0xf9));
            env.set(theme::BUTTON_BORDER_RADIUS, 0.0);
            env.set(theme::BORDER_LIGHT, Color::rgb8(0xd9, 0xd9, 0xd9));
            env.set(theme::BORDER_DARK, Color::rgb8(0x99, 0x99, 0x99));
            env.set(theme::CURSOR_COLOR, Color::rgb8(0x33, 0x33, 0x33));
            env.set(theme::SELECTION_COLOR, Color::rgb8(0x1d, 0x90, 0xcd));
            env.set(theme::PRIMARY_DARK, Color::rgb8(0x1d, 0x90, 0xcd));
            env.set(theme::PRIMARY_LIGHT, Color::rgb8(0x1d, 0x90, 0xcd));
            env.set(theme::SELECTION_COLOR, Color::rgb8(0x1d, 0x90, 0xcd));
            env.set(theme::PROGRESS_BAR_RADIUS, 0.0);
        })
        .use_simple_logger();

    let app_handle = app.get_external_handle();

    let _cam_thread = thread::spawn(move || cam::run_thread(cam_rx, app_handle, window_id));
    //let _render_3d_thread = thread::spawn(move || render_3d::run_thread(render_3d_rx));

    app.launch(initial_state).expect("Failed to launch window");
}

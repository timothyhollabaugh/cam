use super::geometry::MultiPolygon;
use super::PyError;
use crate::cam::geometry::{FromClipper, Geometry, LineString};
use crate::cam::job::OutlinedFilledGeometry;
use pyo3::prelude::*;
use std::path::PathBuf;
use wkt::Wkt;

fn wkt_to_multipolygon(wtk_str: &str) -> Result<MultiPolygon, PyError> {
    let mut wkt = Wkt::from_str(wtk_str)?;

    let multipolygon = MultiPolygon::from_geometry(wkt.items.pop().unwrap())?;

    let mut multilinestring = multipolygon.into_boundary();

    let orientation = multilinestring
        .linestrings
        .iter()
        .max_by_key(|linestring| {
            let (min_point, max_point) = linestring.bounds();
            let size = max_point - min_point;
            (size.x * 10000.0) as u64 * (size.y * 10000.0) as u64
        })
        .map_or(true, |exterior| exterior.clone().into_path().orientation());

    if !orientation {
        for linestring in multilinestring.linestrings.iter_mut() {
            let mut path = linestring.clone().into_path();
            path.reverse();
            *linestring = LineString::from_path(path);
        }
    }

    let paths = multilinestring.clone_paths();
    let multipolygon = MultiPolygon::from_paths(paths);

    Ok(multipolygon)
}

pub fn load_gerber(
    py_cam: &PyModule,
    filename: PathBuf,
) -> Result<OutlinedFilledGeometry, PyError> {
    let (filled_wtk_str, outlined_wtk_str) = py_cam
        .call(
            "load_gerber_wkt",
            (filename.to_string_lossy().into_owned(),),
            None,
        )?
        .extract()?;

    let filled_multipolygon = wkt_to_multipolygon(filled_wtk_str)?;
    let outlined_multipolygon = wkt_to_multipolygon(outlined_wtk_str)?;

    Ok(OutlinedFilledGeometry {
        filled: filled_multipolygon,
        outlined: outlined_multipolygon,
        is_outlined: false,
    })
}

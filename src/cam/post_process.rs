use crate::cam::geometry::Geometry;
use crate::cam::job::{Job, OpDepth, OpSide};
use crate::settings::{Settings, ToolKey, ToolTable};
use itertools::Itertools;
use slotmap::Key;
use std::cmp::Ordering;

pub fn grbl(job: &Job, tools: &ToolTable, settings: &Settings) -> String {
    let mut out = String::new();

    let sorted_tools =
        tools
            .tools
            .iter()
            .enumerate()
            .sorted_by(|(_, (_, tool1)), (_, (_, tool2))| {
                if tool1.diameter > tool2.diameter {
                    Ordering::Greater
                } else if tool1.diameter < tool2.diameter {
                    Ordering::Less
                } else {
                    Ordering::Equal
                }
            });

    let (min_point, _max_point) = job.bounds();

    let min_point = min_point + settings.stock_offset + job.origin();

    let mut last_tool = ToolKey::null();
    let mut last_side = OpSide::Top;

    for (_op_key, op) in job.ops.iter() {
        let mut start_point = min_point;

        if op.side == OpSide::Bottom {
            start_point.mirror(settings.flip_point_1, settings.flip_point_2);
        }

        out += &format!("(Op: {})\n", op.name);
        out += &format!("M3 S0\n");

        if op.side != last_side {
            out += &format!("G0 Z{:.3}\n", settings.clearance_z);
            out += &format!("G0 X{:.3} Y{:.3}\n", settings.loading.x, settings.loading.y);

            out += &format!("M0 (Flip)");

            last_side = op.side;
        }

        for (i, (tool_key, tool)) in sorted_tools.clone() {
            if let Some(Some(paths)) = &op.toolpaths.get(tool_key) {
                out += &format!("(Tool: {})\n", tool.name);
                let cut_z = match op.depth {
                    OpDepth::Through => settings.stock_thickness,
                    OpDepth::Surface => tool.cut_z,
                };

                if tool_key != last_tool {
                    out += &format!("G0 Z{:.3}\n", settings.clearance_z);
                    out += &format!(
                        "G0 X{:.3} Y{:.3}\n",
                        settings.toolchange.x, settings.toolchange.y
                    );

                    out += &format!("M0 ({})\n", tool.name);

                    out += &format!(
                        "G0 X{:.3} Y{:.3}\n",
                        start_point.x - 1.0,
                        start_point.y - 1.0
                    );
                    out += &format!("G0 Z{:.3}\n", settings.toolchange_probe_start_z);
                    out += &format!(
                        "G38.2 Z{:.3} F{:.3}\n",
                        settings.toolchange_probe_end_z, settings.toolchange_probe_feedrate
                    );
                    out += &format!("G10 L20 Z{:.3}\n", settings.toolchange_z_offset);

                    last_tool = tool_key;
                } else {
                    out += &format!("G0 X{:.3} Y{:.3}\n", start_point.x, start_point.y);
                }

                // G01 so bCNC includes in leveling bounds
                out += &format!("G1 Z{:.3} F{:.3}\n", tool.safe_z, tool.z_feedrate);

                out += &format!("M0 (Start)\n");
                out += &format!("M3 S{:.3}\n", tool.spindle_speed);

                let mut paths = paths.clone() + settings.stock_offset + job.origin();

                if op.side == OpSide::Bottom {
                    paths.mirror(settings.flip_point_1, settings.flip_point_2);
                }

                println!("linestrings: {}", paths.linestrings.len());

                for path in paths.linestrings.iter() {
                    let passes = (cut_z / tool.cut_depth) as u32;
                    let zs = (1..=passes).map(|i| i as f64 * tool.cut_depth).chain(
                        if cut_z % tool.cut_depth != 0.0 {
                            Some(cut_z)
                        } else {
                            None
                        },
                    );

                    println!("zs: {:?}", zs.clone().collect::<Vec<_>>());

                    for z in zs {
                        if let Some(first_point) = path.points.head() {
                            out += &format!("G0 X{:.3} Y{:.3}\n", first_point.x, first_point.y,);

                            out += &format!("G1 Z{:.3} F{:.3}\n", -z, tool.z_feedrate);
                            out += &format!("G1 F{:.3}\n", tool.xy_feedrate);

                            for point in path.points.iter().skip(1) {
                                out += &format!("G1 X{:.3} Y{:.3}\n", point.x, point.y);
                            }
                        }
                        out += &format!("G0 Z{:.3}\n", tool.safe_z);
                    }
                }

                out += &format!("M3 S0\n")
            }
        }
    }

    out += &format!("G0 Z{:.3}\n", settings.clearance_z);
    out += &format!("G0 X{:.3} Y{:.3}\n", settings.loading.x, settings.loading.y);

    out
}

pub mod geometry;
pub mod job;
pub mod post_process;
pub mod pycam;
pub mod rscam;

use crate::cam::geometry::MultiLineString;
use crate::cam::job::{
    Job, LoadGeometry, LoadedGeometry, LoadedGeometrySelect, LoadingGeometry, OpKey,
};
use crate::render_3d::TO_RENDER3D;
use crate::settings::{Settings, ToolKey, ToolTable};
use crate::{CamState, Progress, Status};
use crossbeam::channel::Receiver;
use crossbeam::channel::Sender;
use druid::{
    lens, AppDelegate, Command, Data, DelegateCtx, Env, ExtEventSink, Handled, Lens, LensExt,
    Selector, Target,
};
use dxf::DxfError;
use pyo3::prelude::*;
use slotmap::SecondaryMap;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::sync::Arc;

pub const FROM_CAM: Selector<Response> = Selector::new("tim.pycut.from-cam");
pub const TO_CAM: Selector<RequestType> = Selector::new("tim.pycut.to-cam");

const PYTHON_CAM: &str = include_str!("../../py/cam.py");

pub struct CamDelegate {
    tx: Sender<Request>,
}

impl CamDelegate {
    pub fn new(tx: Sender<Request>) -> CamDelegate {
        CamDelegate { tx }
    }
}

impl AppDelegate<CamState> for CamDelegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        cam_state: &mut CamState,
        _env: &Env,
    ) -> Handled {
        if let Some(request) = cmd.get(TO_CAM) {
            self.tx
                .send(Request {
                    request: request.clone(),
                    job: cam_state.job.clone(),
                    tool_table: cam_state.tools.clone(),
                    settings: cam_state.settings.clone(),
                })
                .unwrap();
            Handled::Yes
        } else if let Some(response) = cmd.get(FROM_CAM) {
            match response.clone() {
                Response::Loaded(Ok(geometry)) => {
                    match cam_state.loading_geometry.clone() {
                        LoadGeometry::Loading(LoadingGeometry { filename, target }) => {
                            cam_state.loading_geometry = LoadGeometry::Loaded(LoadedGeometry {
                                filename,
                                target,
                                geometry,
                            })
                        }
                        _ => {}
                    };
                    cam_state.status = Status::Idle;
                }
                Response::Loaded(Err(e)) => {
                    cam_state.status = Status::Error(e);
                }
                Response::Progress(p, msg) => {
                    cam_state.status = Status::Progress(Progress {
                        progress: p,
                        msg: msg.clone(),
                    });
                }
                Response::Cut(op_key, Ok(toolpaths)) => {
                    CamState::job
                        .then(Job::ops)
                        .then(lens::Identity.index(op_key).in_arc())
                        .with_mut(cam_state, |op| {
                            op.toolpaths = Arc::new(toolpaths);
                        });
                    cam_state.status = Status::Idle;
                }
                Response::Cut(_op_key, Err(e)) => {
                    cam_state.status = Status::Error(e);
                }
                Response::ExportedGcode(_) => {}
            }

            Handled::Yes
        } else {
            Handled::No
        }
    }
}

#[derive(Debug)]
pub enum PyError {
    Python(PyErr),
    Cam(CamError),
}

fn py_err_to_cam_err<T>(py_err: Result<T, PyError>) -> Result<Result<T, CamError>, PyErr> {
    match py_err {
        Ok(t) => Ok(Ok(t)),
        Err(PyError::Cam(e)) => Ok(Err(e)),
        Err(PyError::Python(e)) => Err(e),
    }
}

#[derive(Debug, Clone, Data)]
pub enum CamError {
    Wkt(String),

    // Tried to convert a wkt geometry to the wrong `geometry` type
    WrongType,

    // Tried to do something that needs an outline but there is none
    NoOutline,

    // Operation has no geometry loaded
    NoGeometry,

    // Operation not found
    InvalidOp,

    // Not enough tools
    InvalidTool,

    // Drill filename not found
    InvalidDrill,

    // Dxf error
    Dxf(Arc<DxfError>),

    // Unsupported file tupe
    InvalidFileType,
}

impl From<CamError> for PyError {
    fn from(e: CamError) -> Self {
        PyError::Cam(e)
    }
}

impl From<&'static str> for PyError {
    fn from(s: &'static str) -> Self {
        PyError::Cam(CamError::Wkt(s.to_owned()))
    }
}

impl From<&'static str> for CamError {
    fn from(s: &'static str) -> Self {
        CamError::Wkt(s.to_owned())
    }
}

impl From<DxfError> for CamError {
    fn from(e: DxfError) -> Self {
        CamError::Dxf(Arc::new(e))
    }
}

impl From<PyErr> for PyError {
    fn from(e: PyErr) -> Self {
        PyError::Python(e)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum RequestType {
    Load(PathBuf),
    Cut(OpKey),
    ExportGcode(PathBuf),
}

#[derive(Clone, Debug)]
pub struct Request {
    request: RequestType,
    job: Job,
    tool_table: ToolTable,
    settings: Settings,
}

#[derive(Debug, Clone)]
pub enum Response {
    Loaded(Result<LoadedGeometrySelect, CamError>),
    Cut(
        OpKey,
        Result<SecondaryMap<ToolKey, Option<MultiLineString>>, CamError>,
    ),
    Progress(f64, String),
    ExportedGcode(Result<PathBuf, String>),
}

pub fn run_thread(
    rx: Receiver<Request>,
    tx: ExtEventSink,
    target: impl Into<Target> + Copy + 'static,
) -> ! {
    let gil = Python::acquire_gil();
    let py = gil.python();
    let py_cam = match PyModule::from_code(py, PYTHON_CAM, "cam.py", "cam") {
        Ok(m) => m,
        Err(e) => {
            e.print_and_set_sys_last_vars(py);
            panic!("Could not load python module");
        }
    };

    loop {
        let msg = rx.recv().unwrap();

        let response = match handle_msg(py_cam, &tx, msg, target) {
            Ok(r) => r,
            Err(e) => {
                e.print_and_set_sys_last_vars(py);
                continue;
            }
        };

        tx.submit_command(FROM_CAM, response, target).unwrap();
    }
}

fn handle_msg(
    py_cam: &PyModule,
    tx: &ExtEventSink,
    msg: Request,
    target: impl Into<Target> + Copy + 'static,
) -> Result<Response, PyErr> {
    let on_progress = |p: f64, msg: String| {
        tx.submit_command(FROM_CAM, Response::Progress(p, msg), target)
            .unwrap();
    };

    match msg.request {
        RequestType::Load(filename) => {
            let extension = filename
                .extension()
                .map(|e| e.to_string_lossy().to_string());

            if let Some(ext) = extension {
                match ext.as_ref() {
                    "gbr" | "drl" => {
                        let result = pycam::load_gerber(py_cam, filename);
                        py_err_to_cam_err(result)
                            .map(|r| Response::Loaded(r.map(LoadedGeometrySelect::OutlinedFilled)))
                    }

                    "dxf" => Ok(Response::Loaded(
                        rscam::load_dxf(filename).map(LoadedGeometrySelect::Filled),
                    )),
                    "svg" => Ok(Response::Loaded(
                        rscam::load_svg(filename).map(LoadedGeometrySelect::Filled),
                    )),
                    _ => Ok(Response::Loaded(Err(CamError::InvalidFileType))),
                }
            } else {
                Ok(Response::Loaded(Err(CamError::InvalidFileType)))
            }
        }
        RequestType::Cut(op_key) => {
            let op = match msg.job.ops.get(op_key) {
                Some(traces) => traces.clone(),
                None => return Ok(Response::Cut(op_key, Err(CamError::InvalidOp))),
            };

            let tools = op
                .toolpaths
                .iter()
                .filter_map(|(key, _)| {
                    msg.tool_table
                        .tools
                        .get(key)
                        .map(|tool| (key, tool.clone()))
                })
                .collect();

            let result = rscam::cut(
                op.avoids.iter().map(|g| g.geometry.clone()).collect(),
                op.geometry.geometry,
                op.allow_outside_cuts,
                tools,
                op.op_type,
                on_progress,
            );

            Ok(Response::Cut(
                op_key,
                result.map_err(|_| CamError::WrongType),
            ))
        }
        RequestType::ExportGcode(filename) => {
            let gcode = post_process::grbl(&msg.job, &msg.tool_table, &msg.settings);

            let mut file = match File::create(&filename) {
                Ok(f) => f,
                Err(e) => return Ok(Response::ExportedGcode(Err(e.to_string()))),
            };

            match file.write_all(gcode.as_ref()) {
                Ok(_) => Ok(Response::ExportedGcode(Ok(filename.clone()))),
                Err(e) => Ok(Response::ExportedGcode(Err(e.to_string()))),
            }
        }
    }
}

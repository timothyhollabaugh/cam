use super::geometry::{
    difference, intersection, union, Geometry, LineString, MultiLineString, MultiPolygon,
};
use crate::cam::geometry::{FromClipper, Point, Polygon};
use crate::cam::job::{CutType, OpType};
use crate::cam::CamError;
use crate::settings::ToolKey;
use crate::settings::ToolSettings;
use dxf::entities::EntityType;
use dxf::Drawing;
use flo_curves::BezierCurveFactory;
use flo_curves::{BezierCurve, Coord2};
use im::{vector, Vector};
use ordered_float::OrderedFloat;
use slotmap::SecondaryMap;
use std::cmp::Ordering;
use std::f64::consts::PI;
use std::io::Cursor;
use std::path::PathBuf;
use svg::node::element::path::{Command, Data, Position};
use svg::node::element::tag::Path;
use svg::parser::Event;
use usvg::Transform;

const POINTS_PER_CIRCLE: u32 = 64;
const POINTS_PER_SPLINE: u32 = 16;

//4 thou / tooth
//10cm^3/min

pub fn push_points(multilinestring: &mut MultiLineString, points: impl Iterator<Item = Point>) {
    let mut points: Vector<_> = points.collect();

    if let Some(last_linestring) = multilinestring.linestrings.back_mut() {
        if let (Some(existing_first_point), Some(existing_last_point)) = (
            last_linestring.points.front(),
            last_linestring.points.back(),
        ) {
            let new_first_existing_first_close = points
                .front()
                .map(|front| existing_first_point.close(front));

            let new_last_existing_first_close =
                points.front().map(|back| existing_first_point.close(back));

            let new_first_existing_last_close =
                points.front().map(|front| existing_last_point.close(front));

            let new_last_existing_last_close =
                points.back().map(|back| existing_last_point.close(back));

            match (
                new_first_existing_first_close,
                new_last_existing_first_close,
                new_first_existing_last_close,
                new_last_existing_last_close,
            ) {
                (Some(true), _, _, _) => {
                    while let Some(p) = points.pop_front() {
                        last_linestring.points.push_front(p)
                    }
                }
                (_, Some(true), _, _) => {
                    while let Some(p) = points.pop_back() {
                        last_linestring.points.push_front(p)
                    }
                }
                (_, _, Some(true), _) => {
                    while let Some(p) = points.pop_front() {
                        last_linestring.points.push_back(p)
                    }
                }
                (_, _, _, Some(true)) => {
                    while let Some(p) = points.pop_back() {
                        last_linestring.points.push_back(p)
                    }
                }
                (Some(false), Some(false), Some(false), Some(false)) => {
                    last_linestring.close();
                    multilinestring.linestrings.push_back(LineString { points })
                }
                _ => {}
            }
        } else {
            last_linestring.points.extend(points)
        }
    } else {
        multilinestring.linestrings.push_back(LineString { points })
    }
}

pub fn load_dxf(filename: PathBuf) -> Result<MultiPolygon, CamError> {
    let drawing = Drawing::load_file(&filename.to_string_lossy())?;

    let mut multilinestring = MultiLineString::new();

    for e in drawing.entities {
        println!("{:?} on {}", e.specific, e.common.layer);

        match e.specific {
            EntityType::LwPolyline(line) => push_points(
                &mut multilinestring,
                line.vertices.into_iter().map(|v| Point { x: v.x, y: v.y }),
            ),
            EntityType::Circle(circle) => push_points(
                &mut multilinestring,
                (0..POINTS_PER_CIRCLE).map(|i| {
                    let angle = i as f64 * (2.0 * PI / POINTS_PER_CIRCLE as f64);
                    Point {
                        x: circle.center.x + circle.radius * f64::cos(angle),
                        y: circle.center.y + circle.radius * f64::sin(angle),
                    }
                }),
            ),
            EntityType::Arc(arc) => {
                if arc.start_angle > arc.end_angle {
                    let number_points = ((360.0 - (arc.start_angle - arc.end_angle))
                        * POINTS_PER_CIRCLE as f64
                        / 360.0) as usize;

                    push_points(
                        &mut multilinestring,
                        (0..number_points)
                            .map(|i| {
                                let angle = arc.start_angle * PI / 180.0
                                    + i as f64 * (2.0 * PI / POINTS_PER_CIRCLE as f64);
                                Point {
                                    x: arc.center.x + arc.radius * f64::cos(angle),
                                    y: arc.center.y + arc.radius * f64::sin(angle),
                                }
                            })
                            .chain(
                                Some(Point {
                                    x: arc.center.x
                                        + arc.radius * f64::cos(arc.end_angle * PI / 180.0),
                                    y: arc.center.y
                                        + arc.radius * f64::sin(arc.end_angle * PI / 180.0),
                                })
                                .into_iter(),
                            )
                            .rev(),
                    )
                } else {
                    let number_points = ((arc.end_angle - arc.start_angle)
                        * POINTS_PER_CIRCLE as f64
                        / 360.0) as usize;

                    push_points(
                        &mut multilinestring,
                        (0..number_points)
                            .map(|i| {
                                let angle = arc.start_angle * PI / 180.0
                                    + i as f64 * (2.0 * PI / POINTS_PER_CIRCLE as f64);
                                Point {
                                    x: arc.center.x + arc.radius * f64::cos(angle),
                                    y: arc.center.y + arc.radius * f64::sin(angle),
                                }
                            })
                            .chain(
                                Some(Point {
                                    x: arc.center.x
                                        + arc.radius * f64::cos(arc.end_angle * PI / 180.0),
                                    y: arc.center.y
                                        + arc.radius * f64::sin(arc.end_angle * PI / 180.0),
                                })
                                .into_iter(),
                            ),
                    )
                }
            }
            EntityType::Line(line) => push_points(
                &mut multilinestring,
                vec![
                    Point {
                        x: line.p1.x,
                        y: line.p1.y,
                    },
                    Point {
                        x: line.p2.x,
                        y: line.p2.y,
                    },
                ]
                .into_iter(),
            ),
            EntityType::Spline(dxf_spline) => {
                let spline = bspline::BSpline::new(
                    dxf_spline.degree_of_curve as usize,
                    dxf_spline
                        .control_points
                        .iter()
                        .map(|point| Point {
                            x: point.x,
                            y: point.y,
                        })
                        .collect(),
                    dxf_spline
                        .knot_values
                        .into_iter()
                        .map(|knot| knot as f32)
                        .collect(),
                );

                push_points(
                    &mut multilinestring,
                    (0..=POINTS_PER_SPLINE).map(|i| {
                        let t = i as f32 / POINTS_PER_SPLINE as f32;
                        spline.point(t)
                    }),
                );
            }
            _ => unimplemented!(),
        }
    }

    dbg!("Generating multipolygon");

    let paths = multilinestring.clone_paths();
    let multipolygon = MultiPolygon::from_paths(paths);

    dbg!("Generated multipolygon");

    Ok(multipolygon)
}

pub fn load_svg(filename: PathBuf) -> Result<MultiPolygon, CamError> {
    let mut all_multilinestring = MultiLineString::new();

    let tree = usvg::Tree::from_file(filename, &usvg::Options::default()).unwrap();
    let simplified = tree.to_string(usvg::XmlOptions::default());
    println!("{}", simplified);

    for event in svg::read(Cursor::new(simplified)).unwrap() {
        if let Event::Tag(_path, _, attributes) = event {
            if let Some(data) = attributes.get("d") {
                let data = Data::parse(data).unwrap();

                let transform: Option<Transform> = attributes
                    .get("transform")
                    .and_then(|t| t.to_string().parse().ok());

                let mut multilinestring = MultiLineString::new();

                if let Some(last_linestring) = all_multilinestring.linestrings.pop_back() {
                    multilinestring.linestrings.push_back(last_linestring);
                }

                for command in data.iter() {
                    println!("{:?}", command);
                    match command {
                        Command::Move(Position::Absolute, parameters) => {
                            multilinestring.linestrings.push_back(LineString {
                                points: vector![Point {
                                    x: parameters[0] as f64,
                                    y: parameters[1] as f64,
                                }],
                            });
                        }
                        Command::Line(Position::Absolute, parameters) => {
                            multilinestring.back_or_new().points.push_back(Point {
                                x: parameters[0] as f64,
                                y: parameters[1] as f64,
                            })
                        }
                        Command::HorizontalLine(Position::Absolute, parameters) => {
                            let last_point = multilinestring
                                .linestrings
                                .back()
                                .and_then(|linestring| linestring.points.back())
                                .cloned()
                                .unwrap_or(Point { x: 0.0, y: 0.0 });

                            multilinestring.back_or_new().points.push_back(Point {
                                x: parameters[0] as f64,
                                y: last_point.y,
                            });
                        }
                        Command::VerticalLine(Position::Absolute, parameters) => {
                            let last_point = multilinestring
                                .linestrings
                                .back()
                                .and_then(|linestring| linestring.points.back())
                                .cloned()
                                .unwrap_or(Point { x: 0.0, y: 0.0 });

                            multilinestring.back_or_new().points.push_back(Point {
                                x: last_point.x,
                                y: parameters[0] as f64,
                            });
                        }
                        Command::CubicCurve(Position::Absolute, parameters) => {
                            let last_point = multilinestring
                                .linestrings
                                .back()
                                .and_then(|linestring| linestring.points.back())
                                .cloned()
                                .unwrap_or(Point { x: 0.0, y: 0.0 });

                            let bezier = flo_curves::bezier::Curve::from_points(
                                flo_curves::Coord2(last_point.x, last_point.y),
                                (
                                    flo_curves::Coord2(parameters[0] as f64, parameters[1] as f64),
                                    flo_curves::Coord2(parameters[2] as f64, parameters[3] as f64),
                                ),
                                flo_curves::Coord2(parameters[4] as f64, parameters[5] as f64),
                            );

                            multilinestring.back_or_new().points.extend(
                                (0..=POINTS_PER_SPLINE)
                                    .map(|i| i as f64 / POINTS_PER_SPLINE as f64)
                                    .map(|t| bezier.point_at_pos(t))
                                    .map(|Coord2(x, y)| Point { x, y }),
                            );
                        }
                        Command::Close => {
                            if let Some(linestring) = multilinestring.linestrings.back_mut() {
                                linestring.close();
                            }
                        }
                        _ => unimplemented!(),
                    }
                }

                if let Some(transform) = transform {
                    for linestring in multilinestring.linestrings.iter_mut() {
                        for point in linestring.points.iter_mut() {
                            transform.apply_to(&mut point.x, &mut point.y);
                            point.y *= -1.0;
                        }
                    }
                }

                let orientation = multilinestring
                    .linestrings
                    .iter()
                    .max_by_key(|linestring| {
                        let (min_point, max_point) = linestring.bounds();
                        let size = max_point - min_point;
                        (size.x * 10000.0) as u64 * (size.y * 10000.0) as u64
                    })
                    .map_or(true, |exterior| exterior.clone().into_path().orientation());

                if !orientation {
                    for linestring in multilinestring.linestrings.iter_mut() {
                        let mut path = linestring.clone().into_path();
                        path.reverse();
                        *linestring = LineString::from_path(path);
                    }
                }

                dbg!(&multilinestring.linestrings.len());

                all_multilinestring
                    .linestrings
                    .extend(multilinestring.linestrings.into_iter())
            } else {
                println!("No data");
            }
        }
    }

    dbg!("Generating multipolygon");

    let paths = all_multilinestring.clone_paths();
    let multipolygon = MultiPolygon::from_paths(paths);

    dbg!(&multipolygon.polygons.len());

    dbg!("Generated multipolygon");

    Ok(multipolygon)
}

pub fn cut(
    traces: Vector<MultiPolygon>,
    outline: MultiPolygon,
    allow_outside_cuts: bool,
    tools: SecondaryMap<ToolKey, ToolSettings>,
    op_type: OpType,
    on_progress: impl FnMut(f64, String),
) -> Result<SecondaryMap<ToolKey, Option<MultiLineString>>, ()> {
    let path = match op_type {
        OpType::Clear => clear(outline, traces, allow_outside_cuts, tools, on_progress),
        OpType::Cut(cut_type) => cut_outline(outline, traces, tools, cut_type, on_progress),
    };
    let result = path
        .into_iter()
        .map(|(tool_key, path)| (tool_key, Some(path)))
        .collect();

    Ok(result)
}

fn cut_outline(
    outline: MultiPolygon,
    avoid: Vector<MultiPolygon>,
    tools: SecondaryMap<ToolKey, ToolSettings>,
    cut_type: CutType,
    mut on_progress: impl FnMut(f64, String),
) -> SecondaryMap<ToolKey, MultiLineString> {
    on_progress(0.0, "Cutting".to_string());
    let mut sorted_tools = tools.clone().into_iter().collect::<Vec<_>>();

    sorted_tools.sort_by(|(_, tool1), (_, tool2)| {
        if tool1.diameter > tool2.diameter {
            Ordering::Less
        } else if tool1.diameter < tool2.diameter {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });

    if let Some((tool_key, tool)) = sorted_tools.first() {
        let offset = match cut_type {
            CutType::Inside => outline.offset(-tool.diameter / 2.0),
            CutType::On => outline,
            CutType::Outside => outline.offset(tool.diameter / 2.0),
        };

        let mut out = SecondaryMap::new();
        out.insert(tool_key.clone(), offset.into_boundary());
        on_progress(1.0, "Done!".to_string());
        out
    } else {
        on_progress(1.0, "Done!".to_string());
        SecondaryMap::new()
    }
}

fn optimize_paths(
    mut paths: MultiLineString,
    avoid: &MultiPolygon,
    tool: &ToolSettings,
    mut on_progress: impl FnMut(f64, String),
) -> MultiLineString {
    if let Some(first_path) = paths.linestrings.pop_front() {
        let mut ordered_paths = vector![first_path];

        let num_paths = paths.linestrings.len();

        while paths.linestrings.len() > 0 {
            on_progress(
                1.0 - ((paths.linestrings.len() as f64 - 1.0) / num_paths as f64),
                format!("Optimizing {}", tool.name),
            );

            let last_path_point = ordered_paths.back_mut().and_then(|path| {
                path.clone()
                    .points
                    .last()
                    .map(|point| (path, point.clone()))
            });

            if let Some((last_path, last_point)) = last_path_point {
                let closest = paths
                    .linestrings
                    .iter()
                    .enumerate()
                    .flat_map(|(i, path)| path.closest_point(&last_point).map(|path| (i, path)))
                    .min_by_key(|(_, (_, d))| OrderedFloat::from(*d));

                if let Some((path_index, (point_index, _distance))) = closest {
                    let mut closest_path = paths.linestrings.remove(path_index);

                    if let (Some(&start_point), Some(&end_point)) =
                        (closest_path.points.front(), closest_path.points.back())
                    {
                        let almost_closed =
                            (start_point - end_point).distance() <= tool.diameter / 2.0;

                        let ordered_closest = if closest_path.points.len() > 2 && almost_closed {
                            let closed = closest_path.is_closed();

                            closest_path.open();

                            let mut closest_path_vec: Vec<_> =
                                closest_path.points.into_iter().collect();
                            closest_path_vec.as_mut_slice().rotate_left(point_index);
                            let mut ordered_closest = LineString {
                                points: closest_path_vec.into_iter().collect(),
                            };

                            if closed {
                                ordered_closest.close();
                            }

                            ordered_closest
                        } else {
                            let start_distance = (start_point - last_point).distance();
                            let end_distance = (end_point - last_point).distance();

                            if end_distance < start_distance {
                                let mut closest_path_vec: Vec<_> =
                                    closest_path.points.into_iter().collect();
                                closest_path_vec.reverse();
                                let ordered_closest = LineString {
                                    points: closest_path_vec.into_iter().collect(),
                                };

                                ordered_closest
                            } else {
                                closest_path
                            }
                        };

                        if let Some(&first_point) = ordered_closest.points.front() {
                            let rapid = LineString {
                                points: vector![last_point, first_point],
                            };

                            let rapid_length = (first_point - last_point).distance();

                            let cut = rapid.offset(tool.diameter / 2.0);

                            let avoid_cut: MultiLineString = intersection(
                                vec![&cut as &dyn Geometry],
                                vec![avoid as &dyn Geometry],
                            );

                            if !avoid_cut.is_empty() || rapid_length > tool.diameter * 3.0 {
                                ordered_paths.push_back(ordered_closest);
                            } else {
                                last_path.points.append(ordered_closest.points);
                            }
                        }
                        //ordered_paths.push_back(ordered_closest);
                    }
                }
            }
        }

        MultiLineString {
            linestrings: ordered_paths,
        }
    } else {
        paths
    }
}

fn clear_polygons(
    polygons: &MultiPolygon,
    avoid: &MultiPolygon,
    within: Option<&MultiPolygon>,
    tool: &ToolSettings,
    i: usize,
    on_progress: &mut dyn FnMut(f64, String),
) -> MultiLineString {
    let mut paths = MultiLineString {
        linestrings: vector![],
    };

    let tool_radius = tool.diameter / 2.0;

    // TODO: Is this 0.90 causing issues?
    let expanded_avoid = avoid.clone().offset(tool_radius * 0.90);
    let shrunk_within = within.map(|within| within.clone().offset(-tool_radius * 0.90));

    for (poly_index, outline) in polygons.polygons.iter().enumerate() {
        if outline.area() > tool.min_cut_area {
            let progress = poly_index as f64 / polygons.polygons.len() as f64;

            on_progress(progress, format!("Clearing with {}", tool.name));

            let mut child_on_progress = |p: f64, msg: String| {
                let child_p = p / polygons.polygons.len() as f64;
                on_progress(progress + child_p, msg);
            };

            let path = outline.clone().offset(-tool_radius);

            if !path.is_empty() {
                let path = if i > 0 {
                    outline.clone().offset(-tool_radius + tool.overcut)
                } else {
                    path
                };

                let path = path.into_boundary();

                let cut = path.clone().offset(tool_radius);

                let remaining =
                    difference(vec![outline as &dyn Geometry], vec![&cut as &dyn Geometry]);

                let next_paths = clear_polygons(
                    &remaining,
                    avoid,
                    within,
                    tool,
                    i + 1,
                    &mut child_on_progress,
                );

                paths.append(path);
                paths.append(next_paths);
            } else {
                let local_path = outline.clone().into_boundary();

                let cut = local_path.clone().offset(tool_radius);

                let avoid_boundary = avoid.clone().into_boundary();

                let (cut_avoid, _): (MultiLineString, MultiPolygon) = intersection(
                    vec![&avoid_boundary as &dyn Geometry],
                    vec![&cut as &dyn Geometry],
                );

                let avoids_avoid = cut_avoid.is_empty();

                let stays_within = if let Some(within) = within {
                    let (cut_outside1, cut_outside2): (MultiLineString, MultiPolygon) =
                        difference(vec![&cut as &dyn Geometry], vec![within as &dyn Geometry]);
                    cut_outside1.is_empty() && cut_outside2.is_empty()
                } else {
                    true
                };

                if avoids_avoid && stays_within {
                    let useful_cut: MultiPolygon =
                        intersection(vec![&cut as &dyn Geometry], vec![outline as &dyn Geometry]);

                    if useful_cut.area() > tool.min_cut_area {
                        paths.append(local_path);
                    }
                } else {
                    // Generate a bunch of paths that will cut this polygon
                    let local_path = cut_avoid.offset(tool_radius).into_boundary();

                    // Remove parts of the path that would cut avoid areas
                    let (local_path, _): (MultiLineString, MultiPolygon) = difference(
                        vec![&local_path as &dyn Geometry],
                        vec![&expanded_avoid as &dyn Geometry],
                    );

                    // Remove parts of the path that would cut outside the within
                    let local_path = if let Some(ref shrunk_within) = shrunk_within {
                        let (local_path, _): (MultiLineString, MultiPolygon) = intersection(
                            vec![&local_path as &dyn Geometry],
                            vec![shrunk_within as &dyn Geometry],
                        );
                        local_path
                    } else {
                        local_path
                    };

                    // Keep only the parts of the path that actually cut what we want
                    let expanded_outline = outline.clone().offset(tool_radius);
                    let (local_path, _): (MultiLineString, MultiPolygon) = intersection(
                        vec![&local_path as &dyn Geometry],
                        vec![&expanded_outline as &dyn Geometry],
                    );

                    let cut = local_path.clone().offset(tool_radius);

                    let useful_cut: MultiPolygon =
                        intersection(vec![&cut as &dyn Geometry], vec![outline as &dyn Geometry]);

                    if useful_cut.area() > tool.min_cut_area {
                        paths.append(local_path);
                    }
                }
            }
        }
    }

    paths
}

fn cut_path(
    paths: SecondaryMap<ToolKey, MultiLineString>,
    tools: SecondaryMap<ToolKey, ToolSettings>,
) -> MultiPolygon {
    let cuts: Vec<_> = paths
        .into_iter()
        .map(|(tool_key, paths)| {
            let tool = tools.get(tool_key).unwrap();
            paths.offset(tool.diameter / 2.0)
        })
        .collect();

    union(cuts.iter().map(|cut| cut as &dyn Geometry))
}

fn clear(
    outline: MultiPolygon,
    traces: Vector<MultiPolygon>,
    allow_outside_cuts: bool, // Whether to allow cutting outside the outline
    tools: SecondaryMap<ToolKey, ToolSettings>,
    mut on_progress: impl FnMut(f64, String),
) -> SecondaryMap<ToolKey, MultiLineString> {
    let traces = union(traces.iter().map(|g| g as &dyn Geometry));

    let cut_progress_modifier = 0.8;
    let optimize_progress_modifier = 1.0 - cut_progress_modifier;

    let mut sorted_tools = tools.clone().into_iter().collect::<Vec<_>>();

    sorted_tools.sort_by(|(_, tool1), (_, tool2)| {
        if tool1.diameter > tool2.diameter {
            Ordering::Less
        } else if tool1.diameter < tool2.diameter {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });

    let within = if !allow_outside_cuts {
        Some(&outline)
    } else {
        None
    };

    let mut out = SecondaryMap::new();

    if let [(first_key, first_tool), rest @ ..] = sorted_tools.as_slice() {
        let mut on_first_tool_progress = |p: f64, msg: String| {
            let tool_p = p * 1.0 / sorted_tools.len() as f64;
            on_progress(tool_p * cut_progress_modifier, msg)
        };

        let to_cut = difference(
            vec![&outline as &dyn Geometry],
            vec![&traces as &dyn Geometry],
        );

        let paths = clear_polygons(
            &to_cut,
            &traces,
            within,
            first_tool,
            0,
            &mut on_first_tool_progress,
        );
        out.insert(*first_key, paths);

        for (tool_index, (tool_key, tool)) in rest.iter().enumerate() {
            let progress = (tool_index as f64 + 1.0) / sorted_tools.len() as f64;

            let mut on_tool_progress = |p: f64, msg: String| {
                let tool_p = p * 1.0 / sorted_tools.len() as f64;
                on_progress((progress + tool_p) * cut_progress_modifier, msg)
            };

            let to_cut: MultiPolygon = difference(
                vec![&outline as &dyn Geometry],
                vec![&traces as &dyn Geometry],
            );

            let cut_so_far = cut_path(out.clone(), tools.clone());
            let remaining = difference(
                vec![&to_cut as &dyn Geometry],
                vec![&cut_so_far as &dyn Geometry],
            );

            let paths = clear_polygons(&remaining, &traces, within, tool, 0, &mut on_tool_progress);
            out.insert(*tool_key, paths);
        }
    }

    for (index, (tool_key, paths)) in out.iter_mut().enumerate() {
        if let Some(tool) = tools.get(tool_key) {
            let progress = index as f64 / sorted_tools.len() as f64;

            let on_optimize_progress = |p: f64, msg: String| {
                let optimize_p = p / sorted_tools.len() as f64;
                on_progress(
                    cut_progress_modifier + (progress + optimize_p) * optimize_progress_modifier,
                    msg,
                )
            };

            *paths = optimize_paths(paths.clone(), &traces, tool, on_optimize_progress);
        }
    }

    on_progress(1.0, "Done!".to_owned());

    out
}

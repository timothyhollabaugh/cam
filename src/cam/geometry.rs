use druid::{Data, Lens};

use serde::Deserialize;
use serde::Serialize;

//use itertools::Itertools;

use im::vector;
use im::Vector;

use ordered_float::OrderedFloat;

use wkt::types::Coord;
use wkt::types::LineString as WktLineString;
use wkt::types::MultiLineString as WktMultiLineString;
use wkt::types::Point as WktPoint;
use wkt::types::Polygon as WktPolygon;
use wkt::Geometry as WktGeometry;

use clipper_rs::{
    ClipType, Clipper, ClipperOffset, EndType, FillType, JoinType, Point as IntPoint, PolyType,
};
use clipper_rs::{Path, Paths};

use crate::cam::CamError;
use std::collections::VecDeque;

pub const RESOLUTION: f64 = 10000.0;
pub const MITER_LIMIT: f64 = 2.0;
pub const ROUND_PRECISION: f64 = 0.005;

pub trait Geometry {
    fn clone_paths(&self) -> Paths;
    fn has_area(&self) -> bool;
    fn mirror(&mut self, p1: Point, p2: Point);
    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    );
}

pub trait FromClipper {
    type Execute: ClipperExecute;
    fn from_paths(paths: <<Self as FromClipper>::Execute as ClipperExecute>::Output) -> Self;
}

#[derive(Copy, Clone, Debug, PartialEq, Data, Lens, Serialize, Deserialize, Default)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Point {
    pub fn zero() -> Point {
        Point { x: 0.0, y: 0.0 }
    }

    pub fn from_geometry(g: WktGeometry<f64>) -> Result<Point, CamError> {
        if let WktGeometry::Point(WktPoint(Some(c))) = g {
            Ok(Point::from_coord(c))
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn from_coord(c: Coord<f64>) -> Point {
        Point { x: c.x, y: c.y }
    }

    pub fn to_coord(&self) -> Coord<f64> {
        Coord {
            x: self.x,
            y: self.y,
            z: None,
            m: None,
        }
    }

    pub fn from_int_point(point: IntPoint) -> Point {
        Point {
            x: point.x as f64 / RESOLUTION,
            y: point.y as f64 / RESOLUTION,
        }
    }

    pub fn into_int_point(self) -> IntPoint {
        IntPoint {
            x: (self.x * RESOLUTION) as i64,
            y: (self.y * RESOLUTION) as i64,
        }
    }

    pub fn distance(&self) -> f64 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn close(&self, other: &Point) -> bool {
        (self.x - other.x).abs() < 0.01 && (self.y - other.y).abs() < 0.01
    }
}

impl Geometry for Point {
    fn clone_paths(&self) -> Paths {
        Paths::from_points(vec![vec![self.into_int_point()]])
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point, p2: Point) {
        // https://stackoverflow.com/questions/8954326/how-to-calculate-the-mirror-point-along-a-line
        let a = p2.y - p1.y;
        let b = -(p2.x - p1.x);
        let c = -a * p1.x - b * p1.y;

        let m = (a * a + b * b).sqrt();

        let a2 = a / m;
        let b2 = b / m;
        let c2 = c / m;

        let d = a2 * self.x + b2 * self.y + c2;

        self.x -= 2.0 * a2 * d;
        self.y -= 2.0 * b2 * d;
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    ) {
        let lateral_offset = p1_actual - p1_expected;

        let actual_size = p2_actual - p1_actual;
        let actual_angle = f64::atan2(actual_size.y, actual_size.x);

        let expected_size = p2_expected - p1_expected;
        let expected_angle = f64::atan2(expected_size.y, expected_size.x);

        let angular_offset = expected_angle - actual_angle;

        *self += lateral_offset;
    }
}

#[cfg(test)]
mod point_tests {
    use crate::test::*;

    use super::Geometry;
    use super::Point;

    #[test]
    fn transform_by_points_lateral_only() {
        let mut point_to_transform = Point { x: 1.0, y: 1.0 };

        let p1_expected = Point { x: 2.0, y: 3.0 };
        let p1_actual = Point { x: 2.4, y: 3.2 };

        let p2_expected = Point { x: 4.0, y: 5.0 };
        let p2_actual = Point { x: 4.4, y: 5.2 };

        point_to_transform.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);

        assert_close(point_to_transform.x, 1.4);
        assert_close(point_to_transform.y, 1.2);
    }
}

impl std::ops::Neg for Point {
    type Output = Point;

    fn neg(self) -> Self::Output {
        Point {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl std::ops::Sub for Point {
    type Output = Point;

    fn sub(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::Add for Point {
    type Output = Point;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::AddAssign for Point {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl std::ops::Mul<f64> for Point {
    type Output = Point;

    fn mul(self, rhs: f64) -> Self::Output {
        Point {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl std::ops::MulAssign<f64> for Point {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs;
    }
}

impl std::ops::Mul<f32> for Point {
    type Output = Point;
    fn mul(self, rhs: f32) -> Point {
        Point {
            x: self.x * rhs as f64,
            y: self.y * rhs as f64,
        }
    }
}

impl std::ops::Div<f64> for Point {
    type Output = Point;

    fn div(self, rhs: f64) -> Self::Output {
        Point {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl From<Point> for druid::Point {
    fn from(p: Point) -> Self {
        druid::Point::new(p.x, p.y)
    }
}

impl From<druid::Point> for Point {
    fn from(p: druid::Point) -> Self {
        Point { x: p.x, y: p.y }
    }
}

impl From<&Point> for druid::Point {
    fn from(p: &Point) -> Self {
        druid::Point::new(p.x, p.y)
    }
}

impl From<Point> for druid::Vec2 {
    fn from(p: Point) -> Self {
        druid::Vec2::new(p.x, p.y)
    }
}

impl From<druid::Vec2> for Point {
    fn from(p: druid::Vec2) -> Self {
        Point { x: p.x, y: p.y }
    }
}

impl From<druid::Size> for Point {
    fn from(p: druid::Size) -> Self {
        Point {
            x: p.width,
            y: p.height,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Data, Lens)]
pub struct LineString {
    pub points: Vector<Point>,
}

impl LineString {
    pub fn new() -> LineString {
        LineString {
            points: Vector::new(),
        }
    }
    pub fn from_geometry(g: WktGeometry<f64>) -> Result<LineString, CamError> {
        if let WktGeometry::LineString(c) = g {
            Ok(LineString::from_wkt_linesting(c))
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn from_wkt_linesting(ls: WktLineString<f64>) -> LineString {
        LineString {
            points: ls.0.into_iter().map(Point::from_coord).collect(),
        }
    }

    pub fn to_wkt_linestring(&self) -> WktLineString<f64> {
        WktLineString(self.points.iter().map(Point::to_coord).collect())
    }

    pub fn is_closed(&self) -> bool {
        self.points.front() == self.points.back()
    }

    pub fn closed(mut self) -> LineString {
        self.close();
        self
    }

    pub fn close(&mut self) {
        if !self.is_closed() {
            if let Some(front) = self.points.front().cloned() {
                self.points.push_back(front);
            }
        }
    }

    pub fn open(&mut self) {
        if self.is_closed() {
            self.points.pop_back();
        }
    }

    pub fn is_empty(&self) -> bool {
        self.points.is_empty()
    }

    pub fn from_path(path: Path) -> LineString {
        LineString {
            points: path
                .into_points()
                .into_iter()
                .map(Point::from_int_point)
                .collect(),
        }
    }

    pub fn into_path(self) -> Path {
        Path::from_points(self.points.into_iter().map(Point::into_int_point).collect())
    }

    pub fn offset(self, delta: f64) -> Polygon {
        let result = ClipperOffset::new(MITER_LIMIT * RESOLUTION, ROUND_PRECISION * RESOLUTION)
            .with_path(self.into_path(), JoinType::Round, EndType::OpenRound)
            .execute(delta * RESOLUTION);

        let exterior = result
            .into_paths()
            .pop()
            .map(LineString::from_path)
            .unwrap_or(LineString::new());

        Polygon {
            exterior,
            interiors: Vector::new(),
        }
    }

    pub fn bounds(&self) -> (Point, Point) {
        if let Some(first_point) = self.points.head() {
            let mut min_x = first_point.x;
            let mut min_y = first_point.y;
            let mut max_x = first_point.x;
            let mut max_y = first_point.y;

            for point in self.points.iter().skip(1) {
                min_x = min_x.min(point.x);
                min_y = min_y.min(point.y);
                max_x = max_x.max(point.x);
                max_y = max_y.max(point.y);
            }

            (Point { x: min_x, y: min_y }, Point { x: max_x, y: max_y })
        } else {
            (Point { x: 0.0, y: 0.0 }, Point { x: 0.0, y: 0.0 })
        }
    }

    pub fn closest_point(&self, point: &Point) -> Option<(usize, f64)> {
        self.points
            .iter()
            .map(|&p| (p - *point).distance())
            .enumerate()
            .min_by_key(|(_, d)| OrderedFloat::from(*d))
    }

    pub fn clean(&mut self, distance: f64) {
        let closed = self.is_closed();
        let mut path = self.clone().into_path();
        path.clean(distance * RESOLUTION);
        *self = LineString::from_path(path);
        if closed {
            self.close();
        }
    }
}

impl Geometry for LineString {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(vec![self.clone().into_path()])
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point, p2: Point) {
        for p in self.points.iter_mut() {
            p.mirror(p1, p2);
        }

        // Reverse the points to preserve CW/CCW when mirroring so polygon filling works
        for i in 0..self.points.len() / 2 {
            let j = self.points.len() - 1 - i;
            self.points.swap(i, j);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    ) {
        for p in self.points.iter_mut() {
            p.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl std::ops::Add<Point> for LineString {
    type Output = LineString;

    fn add(self, rhs: Point) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point + rhs).collect(),
        }
    }
}

impl std::ops::Add<LineString> for Point {
    type Output = LineString;
    fn add(self, rhs: LineString) -> Self::Output {
        LineString {
            points: rhs.points.into_iter().map(|point| point + self).collect(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Data, Lens)]
pub struct MultiLineString {
    pub linestrings: Vector<LineString>,
}

impl MultiLineString {
    pub fn new() -> MultiLineString {
        MultiLineString {
            linestrings: Vector::new(),
        }
    }

    /// Get a mutable reference to the last LineString, or create a new one and add it if there ar
    /// no linestrings.
    pub fn back_or_new(&mut self) -> &mut LineString {
        if self.is_empty() {
            self.linestrings.push_back(LineString {
                points: Vector::new(),
            });
        }

        self.linestrings.back_mut().unwrap()
    }

    pub fn is_empty(&self) -> bool {
        self.linestrings.len() == 0
    }

    pub fn from_geometry(g: WktGeometry<f64>) -> Result<MultiLineString, CamError> {
        if let WktGeometry::MultiLineString(c) = g {
            Ok(MultiLineString::from_wkt_multilinesting(c))
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn from_wkt_multilinesting(multilinestring: WktMultiLineString<f64>) -> MultiLineString {
        MultiLineString {
            linestrings: multilinestring
                .0
                .into_iter()
                .map(LineString::from_wkt_linesting)
                .collect(),
        }
    }

    pub fn to_wkt_multilinestring(&self) -> WktMultiLineString<f64> {
        WktMultiLineString(
            self.linestrings
                .iter()
                .map(LineString::to_wkt_linestring)
                .collect(),
        )
    }

    pub fn offset(self, delta: f64) -> MultiPolygon {
        let offset = ClipperOffset::new(MITER_LIMIT * RESOLUTION, ROUND_PRECISION * RESOLUTION)
            .with_paths(self.clone_paths(), JoinType::Round, EndType::OpenRound)
            .execute(delta * RESOLUTION);
        MultiPolygon::from_paths(offset)
    }

    pub fn append(&mut self, other: MultiLineString) {
        self.linestrings.append(other.linestrings)
    }

    pub fn bounds(&self) -> (Point, Point) {
        if let Some(first_linestring) = self.linestrings.head() {
            let (mut min_point, mut max_point) = first_linestring.bounds();

            for linestring in self.linestrings.iter().skip(1) {
                let (min_p, max_p) = linestring.bounds();
                min_point.x = min_point.x.min(min_p.x);
                min_point.y = min_point.y.min(min_p.y);
                max_point.x = max_point.x.max(max_p.x);
                max_point.y = max_point.y.max(max_p.y);
            }

            (min_point, max_point)
        } else {
            (Point { x: 0.0, y: 0.0 }, Point { x: 0.0, y: 0.0 })
        }
    }

    pub fn clean(&mut self, distance: f64) {
        for linestring in self.linestrings.iter_mut() {
            linestring.clean(distance);
        }

        self.linestrings
            .retain(|linestring| linestring.points.len() > 0);
    }
}

impl Geometry for MultiLineString {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(
            self.linestrings
                .clone()
                .into_iter()
                .map(LineString::into_path)
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point, p2: Point) {
        for linestring in self.linestrings.iter_mut() {
            linestring.mirror(p1, p2)
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    ) {
        for linestring in self.linestrings.iter_mut() {
            linestring.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl FromClipper for MultiLineString {
    type Execute = ClipperExecutePaths;

    fn from_paths(paths: Paths) -> MultiLineString {
        MultiLineString {
            linestrings: paths
                .into_paths()
                .into_iter()
                .map(LineString::from_path)
                .collect(),
        }
    }
}

impl std::ops::Add<Point> for MultiLineString {
    type Output = MultiLineString;

    fn add(self, rhs: Point) -> Self::Output {
        MultiLineString {
            linestrings: self
                .linestrings
                .into_iter()
                .map(|linestring| linestring + rhs)
                .collect(),
        }
    }
}

impl std::ops::Add<MultiLineString> for Point {
    type Output = MultiLineString;

    fn add(self, rhs: MultiLineString) -> Self::Output {
        MultiLineString {
            linestrings: rhs
                .linestrings
                .into_iter()
                .map(|linestring| linestring + self)
                .collect(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Data, Lens)]
pub struct Polygon {
    pub exterior: LineString,
    pub interiors: Vector<LineString>,
}

impl Polygon {
    pub fn from_geometry(g: WktGeometry<f64>) -> Result<Polygon, CamError> {
        if let WktGeometry::Polygon(polygon) = g {
            Polygon::from_wkt_polygon(polygon)
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn from_wkt_polygon(p: WktPolygon<f64>) -> Result<Polygon, CamError> {
        let mut linestrings = VecDeque::from(p.0);
        if let Some(exterior) = linestrings.pop_front() {
            Ok(Polygon {
                exterior: LineString::from_wkt_linesting(exterior),
                interiors: linestrings
                    .into_iter()
                    .map(LineString::from_wkt_linesting)
                    .collect(),
            })
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn area(&self) -> f64 {
        self.clone().clone_paths().area() / (RESOLUTION * RESOLUTION)
    }

    pub fn into_boundary(self) -> MultiLineString {
        MultiLineString {
            linestrings: vec![self.exterior]
                .into_iter()
                .chain(self.interiors.into_iter())
                .collect(),
        }
    }

    pub fn offset(self, delta: f64) -> MultiPolygon {
        MultiPolygon::from_paths(
            ClipperOffset::new(MITER_LIMIT * RESOLUTION, ROUND_PRECISION * RESOLUTION)
                .with_paths(self.clone_paths(), JoinType::Round, EndType::ClosedPolygon)
                .execute(delta * RESOLUTION),
        )
    }

    pub fn bounds(&self) -> (Point, Point) {
        self.exterior.bounds()
    }

    pub fn clean(&mut self, distance: f64) {
        self.exterior.clean(distance);
        for interior in self.interiors.iter_mut() {
            interior.clean(distance);
        }

        self.interiors
            .retain(|linestring| linestring.points.len() > 0);
    }
}

impl Geometry for Polygon {
    fn clone_paths(&self) -> Paths {
        let mut exterior = self.exterior.clone().into_path();
        if !exterior.orientation() {
            exterior.reverse();
        }
        exterior.orientation();

        let interiors = self.interiors.iter().map(|linestring| {
            let mut interior = linestring.clone().into_path();
            if interior.orientation() {
                interior.reverse();
            }
            interior.orientation();
            interior
        });

        Paths::from_paths(
            vec![exterior]
                .into_iter()
                .chain(interiors.into_iter())
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        true
    }

    fn mirror(&mut self, p1: Point, p2: Point) {
        self.exterior.mirror(p1, p2);

        for interior in self.interiors.iter_mut() {
            interior.mirror(p1, p2);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    ) {
        self.exterior
            .transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);

        for interior in self.interiors.iter_mut() {
            interior.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl std::ops::Add<Point> for Polygon {
    type Output = Polygon;

    fn add(self, rhs: Point) -> Self::Output {
        Polygon {
            exterior: self.exterior + rhs,
            interiors: self
                .interiors
                .into_iter()
                .map(|linestring| linestring + rhs)
                .collect(),
        }
    }
}

impl std::ops::Add<Polygon> for Point {
    type Output = Polygon;

    fn add(self, rhs: Polygon) -> Self::Output {
        Polygon {
            exterior: rhs.exterior + self,
            interiors: rhs
                .interiors
                .into_iter()
                .map(|linestring| linestring + self)
                .collect(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Data, Lens)]
pub struct MultiPolygon {
    pub polygons: Vector<Polygon>,
}

impl MultiPolygon {
    pub fn new() -> MultiPolygon {
        MultiPolygon {
            polygons: Vector::new(),
        }
    }

    pub fn from_geometry(g: WktGeometry<f64>) -> Result<MultiPolygon, CamError> {
        if let WktGeometry::MultiPolygon(multipolygon) = g {
            Ok(MultiPolygon {
                polygons: multipolygon
                    .0
                    .into_iter()
                    .map(Polygon::from_wkt_polygon)
                    .collect::<Result<Vector<Polygon>, CamError>>()?,
            })
        } else if let WktGeometry::Polygon(polygon) = g {
            Ok(MultiPolygon {
                polygons: vector![Polygon::from_wkt_polygon(polygon)?],
            })
        } else {
            Err(CamError::WrongType)
        }
    }

    pub fn is_empty(&self) -> bool {
        self.polygons.len() == 0
    }

    pub fn area(&self) -> f64 {
        self.clone().clone_paths().area() / (RESOLUTION * RESOLUTION)
    }

    pub fn into_boundary(self) -> MultiLineString {
        MultiLineString {
            linestrings: self
                .polygons
                .into_iter()
                .flat_map(|poly| poly.into_boundary().linestrings.into_iter())
                .collect(),
        }
    }

    pub fn offset(self, delta: f64) -> MultiPolygon {
        MultiPolygon::from_paths(
            ClipperOffset::new(MITER_LIMIT * RESOLUTION, ROUND_PRECISION * RESOLUTION)
                .with_paths(self.clone_paths(), JoinType::Round, EndType::ClosedPolygon)
                .execute(delta * RESOLUTION),
        )
    }

    pub fn to_wkt_str(&self) -> String {
        let mut string = "MULTIPOLYGON (".to_string();

        string.push_str(
            &self
                .polygons
                .iter()
                .map(|polygon| {
                    let mut string = String::new();
                    string.push_str("(");

                    string.push_str("(");
                    string.push_str(
                        &polygon
                            .exterior
                            .points
                            .iter()
                            .map(|p| format!("{} {}", p.x, p.y))
                            .intersperse(", ".to_string())
                            .collect::<String>(),
                    );
                    string.push_str(")");

                    for interior in polygon.interiors.iter() {
                        string.push_str(", (");
                        string.push_str(
                            &interior
                                .points
                                .iter()
                                .map(|p| format!("{} {}", p.x, p.y))
                                .intersperse(", ".to_string())
                                .collect::<String>(),
                        );
                        string.push_str(")");
                    }

                    string.push_str(")");

                    string
                })
                .intersperse(", ".to_string())
                .collect::<String>(),
        );

        string.push_str(")");

        string
    }

    pub fn bounds(&self) -> (Point, Point) {
        if let Some(first_polygon) = self.polygons.head() {
            let (mut min_point, mut max_point) = first_polygon.bounds();

            for polygon in self.polygons.iter().skip(1) {
                let (min_p, max_p) = polygon.bounds();
                min_point.x = min_point.x.min(min_p.x);
                min_point.y = min_point.y.min(min_p.y);
                max_point.x = max_point.x.max(max_p.x);
                max_point.y = max_point.y.max(max_p.y);
            }

            (min_point, max_point)
        } else {
            (Point { x: 0.0, y: 0.0 }, Point { x: 0.0, y: 0.0 })
        }
    }

    pub fn clean(&mut self, distance: f64) {
        for polygon in self.polygons.iter_mut() {
            polygon.clean(distance);
        }
    }
}

impl Geometry for MultiPolygon {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(
            self.polygons
                .iter()
                .flat_map(|poly| poly.clone_paths().into_paths().into_iter())
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        true
    }

    fn mirror(&mut self, p1: Point, p2: Point) {
        for polygon in self.polygons.iter_mut() {
            polygon.mirror(p1, p2);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point,
        p1_actual: Point,
        p2_expected: Point,
        p2_actual: Point,
    ) {
        for polygon in self.polygons.iter_mut() {
            polygon.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl FromClipper for MultiPolygon {
    type Execute = ClipperExecutePaths;

    fn from_paths(paths: Paths) -> MultiPolygon {
        let (exteriors, interiors): (Vec<Path>, Vec<Path>) =
            paths.into_paths().into_iter().partition(Path::orientation);

        let multipolygon = MultiPolygon {
            polygons: exteriors
                .into_iter()
                .map(|exterior| {
                    let interior_paths = interiors
                        .iter()
                        .filter(|&interior| {
                            interior
                                .points()
                                .iter()
                                .any(|&point| exterior.point_in_polygon(point).in_or_on())
                        })
                        .cloned();
                    // .map(|path| {
                    //     let mut points = path.into_points();
                    //     if let (Some(first), Some(last)) =
                    //         (points.first().cloned(), points.last().cloned())
                    //     {
                    //         if first != last {
                    //             points.push(first);
                    //         }
                    //     }
                    //     Path::from_points(points)
                    // });

                    let mut clipper = Clipper::new();

                    for path in interior_paths {
                        clipper.add_path(path, PolyType::Subject, true);
                    }

                    let unioned_interior_paths = clipper
                        .execute(ClipType::Union, FillType::NonZero, FillType::NonZero)
                        .unwrap_or(Paths::new());

                    let interiors = MultiLineString::from_paths(unioned_interior_paths);

                    let mut exterior = LineString::from_path(exterior);
                    exterior.close();
                    Polygon {
                        exterior,
                        interiors: interiors
                            .linestrings
                            .into_iter()
                            .map(|mut linestring| {
                                linestring.close();
                                linestring
                            })
                            .collect(),
                    }
                })
                .collect(),
        };

        multipolygon
    }
}

impl std::ops::Add<Point> for MultiPolygon {
    type Output = MultiPolygon;

    fn add(self, rhs: Point) -> Self::Output {
        MultiPolygon {
            polygons: self
                .polygons
                .into_iter()
                .map(|polygon| polygon + rhs)
                .collect(),
        }
    }
}

impl std::ops::Add<MultiPolygon> for Point {
    type Output = MultiPolygon;

    fn add(self, rhs: MultiPolygon) -> Self::Output {
        MultiPolygon {
            polygons: rhs
                .polygons
                .into_iter()
                .map(|polygon| polygon + self)
                .collect(),
        }
    }
}

#[test]
fn test_to_wkt_str() {
    let p = MultiPolygon {
        polygons: vector![
            Polygon {
                exterior: LineString {
                    points: vector![
                        Point { x: 40.0, y: 40.0 },
                        Point { x: 20.0, y: 45.0 },
                        Point { x: 45.0, y: 30.0 },
                        Point { x: 40.0, y: 40.0 },
                    ]
                },
                interiors: Vector::new()
            },
            Polygon {
                exterior: LineString {
                    points: vector![
                        Point { x: 20.0, y: 35.0 },
                        Point { x: 10.0, y: 30.0 },
                        Point { x: 10.0, y: 10.0 },
                        Point { x: 30.0, y: 5.0 },
                        Point { x: 45.0, y: 20.0 },
                        Point { x: 20.0, y: 35.0 },
                    ]
                },
                interiors: vector![LineString {
                    points: vector![
                        Point { x: 30.0, y: 20.0 },
                        Point { x: 20.0, y: 15.0 },
                        Point { x: 20.0, y: 25.0 },
                        Point { x: 30.0, y: 20.0 },
                    ]
                }]
            }
        ],
    };

    assert_eq!(
        p.to_wkt_str(),
        "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)), ((20 35, 10 30, 10 10, 30 5, 45 20, 20 35), (30 20, 20 15, 20 25, 30 20)))".to_string()
    )
}

impl FromClipper for (MultiLineString, MultiPolygon) {
    type Execute = ClipperExecuteTree;

    fn from_paths((open_paths, closed_paths): (Paths, Paths)) -> Self {
        (
            MultiLineString::from_paths(open_paths),
            MultiPolygon::from_paths(closed_paths),
        )
    }
}

pub trait ClipperExecute {
    type Output;
    fn execute(clipper: Clipper, clip_type: ClipType) -> Self::Output;
}

pub struct ClipperExecutePaths;

impl ClipperExecute for ClipperExecutePaths {
    type Output = Paths;
    fn execute(clipper: Clipper, clip_type: ClipType) -> Paths {
        clipper
            .execute(clip_type, FillType::NonZero, FillType::NonZero)
            .unwrap_or(Paths::new())
    }
}

pub struct ClipperExecuteTree;

impl ClipperExecute for ClipperExecuteTree {
    type Output = (Paths, Paths);

    fn execute(clipper: Clipper, clip_type: ClipType) -> (Paths, Paths) {
        clipper
            .execute_tree(clip_type, FillType::NonZero, FillType::NonZero)
            .unwrap_or((Paths::new(), Paths::new()))
    }
}

pub fn union<'i, I, O>(paths: I) -> O
where
    I: IntoIterator<Item = &'i dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in paths {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Union);

    O::from_paths(out)
}

pub fn difference<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Difference);

    O::from_paths(out)
}

pub fn intersection<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Intersection);

    O::from_paths(out)
}

#[allow(dead_code)]
pub fn xor<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Xor);

    O::from_paths(out)
}

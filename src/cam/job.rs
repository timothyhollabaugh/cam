use super::geometry::{MultiLineString, MultiPolygon, Point};
use crate::settings::ToolKey;
use druid::{Data, Lens};
use slotmap::{new_key_type, DenseSlotMap, SecondaryMap};
use std::path::PathBuf;
use std::sync::Arc;

#[derive(Clone, Data, PartialEq, Debug)]
pub enum GeometryTarget {
    Op,
    Avoid(OpKey),
}

#[derive(Clone, Data, PartialEq, Debug)]
pub enum LoadGeometry {
    /// We are not loading geometry right now
    None,

    /// Some geometry is actively being loaded by the other thread
    Loading(LoadingGeometry),

    /// The geometry has been loaded and is awaiting user OK
    Loaded(LoadedGeometry),
}

#[derive(Clone, Data, Debug, PartialEq, Lens)]
pub struct LoadingGeometry {
    pub filename: Arc<PathBuf>,
    pub target: GeometryTarget,
}

/// A loaded geometry from a gerber file that could
/// be filled out outlined. Need to ask the user.
#[derive(Clone, Data, Debug, PartialEq, Lens)]
pub struct OutlinedFilledGeometry {
    pub outlined: MultiPolygon,
    pub filled: MultiPolygon,
    pub is_outlined: bool,
}

/// A response from loading geometry. May need to
/// ask the user for clarification (eg filled or
/// outlined)
#[derive(Clone, Data, Debug, PartialEq)]
pub enum LoadedGeometrySelect {
    OutlinedFilled(OutlinedFilledGeometry),
    Filled(MultiPolygon),
}

#[derive(Clone, Data, Debug, PartialEq, Lens)]
pub struct LoadedGeometry {
    pub filename: Arc<PathBuf>,
    pub target: GeometryTarget,
    pub geometry: LoadedGeometrySelect,
}

impl From<LoadedGeometry> for OpGeometry {
    fn from(loaded_geometry: LoadedGeometry) -> OpGeometry {
        let geometry = match loaded_geometry.geometry {
            LoadedGeometrySelect::OutlinedFilled(outlined_filled) => {
                if outlined_filled.is_outlined {
                    outlined_filled.outlined
                } else {
                    outlined_filled.filled
                }
            }
            LoadedGeometrySelect::Filled(geometry) => geometry,
        };

        OpGeometry {
            filename: loaded_geometry.filename,
            geometry,
        }
    }
}

#[derive(Clone, Data, Lens, Debug, PartialEq)]
pub struct OpGeometry {
    pub filename: Arc<PathBuf>,
    pub geometry: MultiPolygon,
}

#[derive(Copy, Clone, Data, Debug, PartialEq)]
pub enum OpType {
    Clear, // TODO: Allow specifying if the clear is allowed to extend past the bounds
    Cut(CutType),
}

#[derive(Copy, Clone, Data, Debug, PartialEq)]
pub enum CutType {
    Inside,
    On,
    Outside,
}

#[derive(Copy, Clone, Data, Debug, PartialEq)]
pub enum OpDepth {
    Surface,
    Through,
}

#[derive(Copy, Clone, Data, Debug, PartialEq)]
pub enum OpSide {
    Top,
    Bottom,
}

#[derive(Clone, Data, Lens, Debug, PartialEq)]
pub struct Op {
    pub name: String,
    pub side: OpSide,
    pub geometry: OpGeometry,
    pub op_type: OpType,
    pub depth: OpDepth,
    pub allow_outside_cuts: bool,
    pub toolpaths: Arc<SecondaryMap<ToolKey, Option<MultiLineString>>>,
    pub avoids: Arc<Vec<OpGeometry>>,
}

new_key_type! {
    pub struct OpKey;
}

impl Data for OpKey {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

#[derive(Clone, Data, Lens, Debug)]
pub struct Job {
    pub offset: Point,
    pub ops: Arc<DenseSlotMap<OpKey, Op>>,
}

impl Job {
    pub fn new() -> Job {
        Job {
            offset: Point::zero(),
            ops: Arc::new(DenseSlotMap::with_key()),
        }
    }

    pub fn bounds(&self) -> (Point, Point) {
        let mut min_point = Point {
            x: f64::INFINITY,
            y: f64::INFINITY,
        };
        let mut max_point = Point {
            x: f64::NEG_INFINITY,
            y: f64::NEG_INFINITY,
        };

        for (_, op) in self.ops.iter() {
            let (min_p, max_p) = op.geometry.geometry.bounds();
            min_point.x = min_point.x.min(min_p.x);
            min_point.y = min_point.y.min(min_p.y);
            max_point.x = max_point.x.max(max_p.x);
            max_point.y = max_point.y.max(max_p.y);
        }

        (min_point, max_point)
    }

    pub fn origin(&self) -> Point {
        -self.bounds().0 + self.offset
    }
}

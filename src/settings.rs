use std::fmt::Debug;
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::Path;
use std::sync::Arc;

use lazy_static::lazy_static;

use pyo3::types::PyDict;
use pyo3::Python;

use serde::{Deserialize, Serialize};

use druid::{Data, Lens};

use slotmap::{new_key_type, DenseSlotMap};

use crate::cam::geometry::Geometry;
use crate::cam::geometry::Point;
use directories_next::ProjectDirs;

lazy_static! {
    static ref DIRS: Option<ProjectDirs> = ProjectDirs::from("", "", "cam");
}

pub const TOOLS_FILENAME: &'static str = "tools.toml";
pub const SETTINGS_FILENAME: &'static str = "settings.toml";

#[derive(Clone, Data, Lens, Debug, PartialEq, Serialize, Deserialize)]
pub struct ToolSettings {
    pub name: String,
    pub diameter: f64,
    pub overcut: f64,
    pub min_cut_area: f64,
    pub safe_z: f64,
    pub cut_z: f64,
    pub z_feedrate: f64,
    pub xy_feedrate: f64,
    pub cut_depth: f64,
    pub spindle_speed: f64,
    pub color: (f64, f64, f64),
}

impl ToolSettings {
    pub fn make_python(&self) -> Box<dyn FnOnce(Python) -> &PyDict + '_> {
        let name = self.name.clone();
        Box::new(move |py: Python| {
            let dict = PyDict::new(py);
            dict.set_item("name", name).unwrap();
            dict.set_item("diameter", self.diameter).unwrap();
            dict.set_item("overcut", self.overcut).unwrap();
            dict.set_item("min_cut_area", self.min_cut_area).unwrap();
            dict.set_item("safe_z", self.safe_z).unwrap();
            dict.set_item("cut_z", self.cut_z).unwrap();
            dict.set_item("z_feedrate", self.z_feedrate).unwrap();
            dict.set_item("xy_feedrate", self.xy_feedrate).unwrap();
            dict.set_item("cut_depth", self.cut_depth).unwrap();
            dict
        })
    }
}

impl Default for ToolSettings {
    fn default() -> Self {
        ToolSettings {
            name: "New Tool".to_string(),
            diameter: 1.0,
            overcut: 0.0,
            min_cut_area: 0.0,
            safe_z: 1.0,
            cut_z: -0.1,
            z_feedrate: 500.0,
            xy_feedrate: 500.0,
            cut_depth: 0.1,
            spindle_speed: 0.0,
            color: (0.5, 0.5, 0.5),
        }
    }
}

new_key_type! {
    pub struct ToolKey;
}

impl Data for ToolKey {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

#[derive(Serialize, Deserialize, Clone, Data, Lens, Debug, Default)]
pub struct ToolTable {
    pub tools: Arc<DenseSlotMap<ToolKey, ToolSettings>>,
}

#[derive(Clone, Data, Lens, Debug, Serialize, Deserialize, Default)]
pub struct Settings {
    pub loading: Point,
    pub clearance_z: f64,
    pub stock_size: Point,
    pub stock_thickness: f64,
    pub stock_offset: Point,
    pub stock_offset_z: f64,
    pub flip_point_1: Point,
    pub flip_point_2: Point,
    pub toolchange: Point,
    pub toolchange_probe_start_z: f64,
    pub toolchange_probe_end_z: f64,
    pub toolchange_probe_feedrate: f64,
    pub toolchange_z_offset: f64,
}

pub fn load<T: for<'a> Deserialize<'a> + Default>(name: &str) -> T {
    DIRS.as_ref()
        .map(|d| d.config_dir())
        .and_then(|d| File::open(d.join(Path::new(name))).ok())
        .and_then(|file| ron::de::from_reader(file).ok())
        .unwrap_or_default()
}

pub fn save<T: Serialize>(name: &str, data: &T) {
    let serialized_str = ron::ser::to_string_pretty(data, ron::ser::PrettyConfig::new()).unwrap();

    let dir = DIRS.as_ref().unwrap().config_dir();
    let path = dir.join(Path::new(name));

    create_dir_all(dir).unwrap();

    let mut file = File::create(path).unwrap();
    file.write_all(serialized_str.as_bytes())
        .expect("Could not save");
}

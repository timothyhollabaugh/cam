use crate::cam::geometry::Polygon;
use crate::cam::job::Job;
use crate::CamState;
use crossbeam::channel::unbounded;
use crossbeam::channel::{Receiver, Sender};
use druid::{AppDelegate, Command, DelegateCtx, Env, Handled, Selector, Target};
use kiss3d::camera::ArcBall;
use kiss3d::light::Light;
use kiss3d::nalgebra::{Point3, Vector3};
use kiss3d::ncollide3d::shape::TriMesh;
use kiss3d::resource::Mesh as Kiss3dMesh;
use kiss3d::window::Window;
use std::cell::RefCell;
use std::rc::Rc;
use std::thread;

pub const TO_RENDER3D: Selector<()> = Selector::new("tim.pycut.to-render3d");

pub struct Render3dDelegate {
    tx: Sender<Job>,
}

impl Render3dDelegate {
    pub fn new(tx: Sender<Job>) -> Render3dDelegate {
        Render3dDelegate { tx }
    }
}

impl AppDelegate<CamState> for Render3dDelegate {
    fn command(
        &mut self,
        _cts: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        cam_state: &mut CamState,
        _env: &Env,
    ) -> Handled {
        if let Some(()) = cmd.get(TO_RENDER3D) {
            self.tx.send(cam_state.job.clone());
            Handled::Yes
        } else {
            Handled::No
        }
    }
}

fn triangulate(polygon: &Polygon) -> Option<delaunator::Triangulation> {
    let points: Vec<_> = polygon
        .exterior
        .points
        .iter()
        .map(|p| delaunator::Point { x: p.x, y: p.y })
        .collect();

    delaunator::triangulate(&points)
}

pub fn run_thread(rx: Receiver<Job>) {
    let mut window = Window::new("Cam");
    window.add_cube(20.0, 20.0, 0.0);
    window.set_light(Light::StickToCamera);
    let mut camera = ArcBall::new(Point3::new(50.0, 50.0, 50.0), Point3::new(0.0, 0.0, 0.0));
    camera.set_up_axis_dir(Vector3::z_axis());
    let mut meshes = Vec::new();
    while window.render_with_camera(&mut camera) {
        if let Ok(job) = rx.try_recv() {
            dbg!(&job);

            while let Some(mut mesh) = meshes.pop() {
                window.remove_node(&mut mesh);
            }

            for (op_key, op) in job.ops.iter() {
                for polygon in &op.geometry.geometry.polygons {
                    if let Some(triangulation) = triangulate(polygon) {
                        let vertices: Vec<_> = polygon
                            .exterior
                            .points
                            .iter()
                            .map(|p| Point3::new(p.x as f32, p.y as f32, 0.0))
                            .collect();

                        let faces: Vec<_> = triangulation
                            .triangles
                            .array_chunks::<3>()
                            .cloned()
                            .map(|[a, b, c]| Point3::new(a as u16, b as u16, c as u16))
                            .collect();

                        println!("vertices: {:?}", &vertices);
                        println!("faces: {:?}", &faces);

                        // let mesh =
                        //     Kiss3dMesh::new(vertices.clone(), faces.clone(), None, None, false);
                        // let scene_node = window
                        //     .add_mesh(Rc::new(RefCell::new(mesh)), From::from([1.0, 1.0, 1.0]));
                        // meshes.push(scene_node);

                        let flipped_mesh = Kiss3dMesh::new(
                            vertices.into_iter().rev().collect(),
                            faces.into_iter().rev().collect(),
                            None,
                            None,
                            false,
                        );
                        let scene_node = window.add_mesh(
                            Rc::new(RefCell::new(flipped_mesh)),
                            From::from([1.0, 1.0, 1.0]),
                        );
                        meshes.push(scene_node);
                    }
                }
            }
        }
    }
}
